//
//  MenuControllerVC.swift
//  SidebarMenu
//
//  Created by Apple  on 16/10/19.
//  Copyright © 2019 AppCoda. All rights reserved.
//

import UIKit
import Alamofire

import SDWebImage

class MenuControllerVC: UIViewController {

    let cellReuseIdentifier = "menuControllerVCTableCell"
    
    var bgImage: UIImageView?
    
    var roleIs:String!
    
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_COLOR
        }
    }
    
    @IBOutlet weak var viewUnderNavigation:UIView! {
        didSet {
            // viewUnderNavigation.backgroundColor = .black
            viewUnderNavigation.backgroundColor = .white
        }
    }
    
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "MENU"
            lblNavigationTitle.textColor = NAVIGATION_TITLE_COLOR
        }
    }
    
    @IBOutlet weak var imgSidebarMenuImage:UIImageView! {
        didSet {
            imgSidebarMenuImage.backgroundColor = .clear
            imgSidebarMenuImage.layer.cornerRadius = 30
            imgSidebarMenuImage.clipsToBounds = true
        }
    }
    
    // driver / customer
    var arrSideBarMenuTitle = ["Home", "Edit Profile", "Change Password", "Scan Report", "Help", "Privacy Policy", "Terms of Use", "Sign out"]
    
    var arrSideBarMenuImage = ["home", "edit1", "lock", "privacy", "help", "privacy", "privacy", "logout"]
    
    
    @IBOutlet weak var lblUserName:UILabel! {
        didSet {
            lblUserName.text = "JOHN SMITH"
            lblUserName.textColor = .black
        }
    }
    @IBOutlet weak var lblPhoneNumber:UILabel! {
        didSet {
            lblPhoneNumber.textColor = .black
        }
    }
    
    @IBOutlet var menuButton:UIButton!
    
    @IBOutlet weak var tbleView: UITableView! {
        didSet {
            tbleView.delegate = self
            tbleView.dataSource = self
            tbleView.tableFooterView = UIView.init()
            tbleView.backgroundColor = NAVIGATION_COLOR
            // tbleView.separatorInset = UIEdgeInsets(top: 0, left: 50, bottom: 0, right: 50)
            tbleView.separatorColor = .white
        }
    }
    
    @IBOutlet weak var lblMainTitle:UILabel!
    @IBOutlet weak var lblAddress:UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        sideBarMenuClick()
        view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        
        self.tbleView.separatorColor = .white // UIColor.init(red: 60.0/255.0, green: 110.0/255.0, blue: 160.0/255.0, alpha: 1)
        
        self.view.backgroundColor = .white // NAVIGATION_BACKGROUND_COLOR
        
        self.sideBarMenuClick()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            
             
                
            self.lblUserName.text = "Welcome"
            
            
            
            self.lblPhoneNumber.text = (person["fullName"] as! String)
                
            self.imgSidebarMenuImage.sd_imageIndicator = SDWebImageActivityIndicator.whiteLarge
            self.imgSidebarMenuImage.sd_setImage(with: URL(string: (person["image"] as! String)), placeholderImage: UIImage(named: "logo"))
             
             
            // self.imgSidebarMenuImage.sd_setImage(with: URL(string: "https://firebasestorage.googleapis.com/v0/b/ahhcookoo.appspot.com/o/singleChatImage%2F123.png?alt=media&token=c02cac95-f9c1-4ffd-9110-28cf31ce17be"), placeholderImage: UIImage(named: "logo"))
            
        }
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
          return .lightContent
    }
    
    @objc func sideBarMenuClick() {
        
        if revealViewController() != nil {
        menuButton.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
        
            revealViewController().rearViewRevealWidth = 300
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
          }
    }
}

extension MenuControllerVC: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrSideBarMenuTitle.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:MenuControllerVCTableCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! MenuControllerVCTableCell
        
        cell.backgroundColor = .clear
      
        cell.separatorInset = UIEdgeInsets.zero
        cell.layoutMargins = UIEdgeInsets.zero
        
        cell.lblName.text = arrSideBarMenuTitle[indexPath.row]
        cell.lblName.textColor = .white
        cell.imgProfile.backgroundColor = .orange
        cell.imgProfile.image = UIImage(named: arrSideBarMenuImage[indexPath.row])
        cell.imgProfile.backgroundColor = .clear
                 
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    
        if arrSideBarMenuTitle[indexPath.row] == "Home" {
                    
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let sw = storyboard.instantiateViewController(withIdentifier: "sw") as! SWRevealViewController
            self.view.window?.rootViewController = sw
            let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "DashboardId")
            let navigationController = UINavigationController(rootViewController: destinationController!)
            sw.setFront(navigationController, animated: true)
                    
        } else if arrSideBarMenuTitle[indexPath.row] == "Edit Profile" {
            
            let myString = "backOrMenu"
            UserDefaults.standard.set(myString, forKey: "keyBackOrSlide")
                    
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let sw = storyboard.instantiateViewController(withIdentifier: "sw") as! SWRevealViewController
            self.view.window?.rootViewController = sw
            let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "CEditProfileId")
            let navigationController = UINavigationController(rootViewController: destinationController!)
            sw.setFront(navigationController, animated: true)
                    
        } else if arrSideBarMenuTitle[indexPath.row] == "Change Password" {
            
            let myString = "backOrMenu"
            UserDefaults.standard.set(myString, forKey: "keyBackOrSlide")
                    
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let sw = storyboard.instantiateViewController(withIdentifier: "sw") as! SWRevealViewController
            self.view.window?.rootViewController = sw
            let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "ChangePasswordId")
            let navigationController = UINavigationController(rootViewController: destinationController!)
            sw.setFront(navigationController, animated: true)
                    
        } else if arrSideBarMenuTitle[indexPath.row] == "Scan Report" {
            
            let myString = "backOrMenu"
            UserDefaults.standard.set(myString, forKey: "keyBackOrSlide")
                    
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let sw = storyboard.instantiateViewController(withIdentifier: "sw") as! SWRevealViewController
            self.view.window?.rootViewController = sw
            let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "FormBagHistoryId")
            let navigationController = UINavigationController(rootViewController: destinationController!)
            sw.setFront(navigationController, animated: true)
                    
        } else if arrSideBarMenuTitle[indexPath.row] == "Help" {
            
            let myString = "backOrMenu"
            UserDefaults.standard.set(myString, forKey: "keyBackOrSlide")
                    
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let sw = storyboard.instantiateViewController(withIdentifier: "sw") as! SWRevealViewController
            self.view.window?.rootViewController = sw
            let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "HelpId")
            let navigationController = UINavigationController(rootViewController: destinationController!)
            sw.setFront(navigationController, animated: true)
                    
        } else if arrSideBarMenuTitle[indexPath.row] == "Privacy Policy" {
            
            let myString = "backOrMenu"
            UserDefaults.standard.set(myString, forKey: "keyBackOrSlide")
                    
            let myString2 = "privacyPolicy"
            UserDefaults.standard.set(myString2, forKey: "keySelect")
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let sw = storyboard.instantiateViewController(withIdentifier: "sw") as! SWRevealViewController
            self.view.window?.rootViewController = sw
            let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "TermsAndPrivacyId")
            let navigationController = UINavigationController(rootViewController: destinationController!)
            sw.setFront(navigationController, animated: true)
                    
        } else if arrSideBarMenuTitle[indexPath.row] == "Terms of Use" {
            
            let myString = "backOrMenu"
            UserDefaults.standard.set(myString, forKey: "keyBackOrSlide")
                    
            let myString2 = "termsOfUse"
            UserDefaults.standard.set(myString2, forKey: "keySelect")
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let sw = storyboard.instantiateViewController(withIdentifier: "sw") as! SWRevealViewController
            self.view.window?.rootViewController = sw
            let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "TermsAndPrivacyId")
            let navigationController = UINavigationController(rootViewController: destinationController!)
            sw.setFront(navigationController, animated: true)
                    
        } else if arrSideBarMenuTitle[indexPath.row] == "Sign out" {
            
            let alert = UIAlertController(title: String("Sign out"), message: String("Are you sure you want to Sign out ?"), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Yes, Sign out", style: .default, handler: { action in
                self.logoutWB()
            }))
            alert.addAction(UIAlertAction(title: "Dismiss", style: .destructive, handler: { action in
            }))
            self.present(alert, animated: true, completion: nil)
                    
        }
        
        
        
        /*
        if arrSideBarMenuTitle[indexPath.row] == "Home" {
                    
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let sw = storyboard.instantiateViewController(withIdentifier: "sw") as! SWRevealViewController
            self.view.window?.rootViewController = sw
            let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "DashbaordId")
            let navigationController = UINavigationController(rootViewController: destinationController!)
            sw.setFront(navigationController, animated: true)
                    
        } else if arrSideBarMenuTitle[indexPath.row] == "Edit Profile" {
                    
            let myString = "backOrMenu"
            UserDefaults.standard.set(myString, forKey: "keyBackOrSlide")
                    
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let sw = storyboard.instantiateViewController(withIdentifier: "sw") as! SWRevealViewController
            self.view.window?.rootViewController = sw
            let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "CEditProfileId")
            let navigationController = UINavigationController(rootViewController: destinationController!)
            sw.setFront(navigationController, animated: true)
                    
        } else if arrSideBarMenuTitle[indexPath.row] == "Near By" {
            
            let myString = "backOrMenu"
            UserDefaults.standard.set(myString, forKey: "keyBackOrSlide")
                    
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let sw = storyboard.instantiateViewController(withIdentifier: "sw") as! SWRevealViewController
            self.view.window?.rootViewController = sw
            let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "FindVendorId")
            let navigationController = UINavigationController(rootViewController: destinationController!)
            sw.setFront(navigationController, animated: true)
                    
        } else if arrSideBarMenuTitle[indexPath.row] == "Privacy Policy" {
            
            let myString = "backOrMenu"
            UserDefaults.standard.set(myString, forKey: "keyBackOrSlide")
                    
            let myString2 = "privacyPolicy"
            UserDefaults.standard.set(myString2, forKey: "keySelect")
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let sw = storyboard.instantiateViewController(withIdentifier: "sw") as! SWRevealViewController
            self.view.window?.rootViewController = sw
            let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "TermsAndPrivacyId")
            let navigationController = UINavigationController(rootViewController: destinationController!)
            sw.setFront(navigationController, animated: true)
                    
        } else if arrSideBarMenuTitle[indexPath.row] == "Terms of Use" {
            
            let myString = "backOrMenu"
            UserDefaults.standard.set(myString, forKey: "keyBackOrSlide")
                    
            let myString2 = "termsOfUse"
            UserDefaults.standard.set(myString2, forKey: "keySelect")
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let sw = storyboard.instantiateViewController(withIdentifier: "sw") as! SWRevealViewController
            self.view.window?.rootViewController = sw
            let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "TermsAndPrivacyId")
            let navigationController = UINavigationController(rootViewController: destinationController!)
            sw.setFront(navigationController, animated: true)
                    
        } else if arrSideBarMenuTitle[indexPath.row] == "Help" {
            
            let myString = "backOrMenu"
            UserDefaults.standard.set(myString, forKey: "keyBackOrSlide")
                    
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let sw = storyboard.instantiateViewController(withIdentifier: "sw") as! SWRevealViewController
            self.view.window?.rootViewController = sw
            let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "HelpId")
            let navigationController = UINavigationController(rootViewController: destinationController!)
            sw.setFront(navigationController, animated: true)
                    
        } else if arrSideBarMenuTitle[indexPath.row] == "Change Password" {
            
            let myString = "backOrMenu"
            UserDefaults.standard.set(myString, forKey: "keyBackOrSlide")
                    
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let sw = storyboard.instantiateViewController(withIdentifier: "sw") as! SWRevealViewController
            self.view.window?.rootViewController = sw
            let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "ChangePasswordId")
            let navigationController = UINavigationController(rootViewController: destinationController!)
            sw.setFront(navigationController, animated: true)
                    
        } else if arrSideBarMenuTitle[indexPath.row] == "Sign out" {
                    
            let alert = UIAlertController(title: String("Sign out"), message: String("Are you sure you want to Sign out ?"), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Yes, Sign out", style: .default, handler: { action in
                self.logoutWB()
            }))
            alert.addAction(UIAlertAction(title: "Dismiss", style: .destructive, handler: { action in
            }))
            self.present(alert, animated: true, completion: nil)
                    
        }
        */
    }
    
    /*
     pradeep.bamola@evirtualservices.com
     pradeep@#$12@

     */
    
    @objc func logoutWB() {
         ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
        
        self.view.endEditing(true)
        
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
         // let str:String = person["role"] as! String
        
            let x : Int = person["userId"] as! Int
            let myString = String(x)
            
        let params = LogoutFromApp(action: "logout",
                                   userId: String(myString))
        
        AF.request(APPLICATION_BASE_URL,
                   method: .post,
                   parameters: params,
                   encoder: JSONParameterEncoder.default).responseJSON { response in
                    // debugPrint(response.result)
                    
                    switch response.result {
                    case let .success(value):
                        
                        let JSON = value as! NSDictionary
                          // print(JSON as Any)
                        
                        var strSuccess : String!
                        strSuccess = JSON["status"]as Any as? String
                        
                        // var strSuccess2 : String!
                        // strSuccess2 = JSON["msg"]as Any as? String
                        
                        if strSuccess == String("success") {
                            print("yes")
                            ERProgressHud.sharedInstance.hide()
                           
                            let defaults = UserDefaults.standard
                            defaults.setValue("", forKey: "keyLoginFullData")
                            defaults.setValue(nil, forKey: "keyLoginFullData")

                            defaults.setValue("", forKey: "rememberMefoReal")
                            defaults.setValue(nil, forKey: "rememberMefoReal")
 
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let sw = storyboard.instantiateViewController(withIdentifier: "sw") as! SWRevealViewController
                            self.view.window?.rootViewController = sw
                            let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "ContinueAsAId")
                            let navigationController = UINavigationController(rootViewController: destinationController!)
                            sw.setFront(navigationController, animated: true)
                            
                            
                        } else {
                            
                            print("no")
                            ERProgressHud.sharedInstance.hide()
                            
                            var strSuccess2 : String!
                            strSuccess2 = JSON["msg"]as Any as? String
                            
                            Utils.showAlert(alerttitle: String(strSuccess), alertmessage: String(strSuccess2), ButtonTitle: "Ok", viewController: self)
                            
                        }
                        
                    case let .failure(error):
                        print(error)
                        ERProgressHud.sharedInstance.hide()
                        
                        Utils.showAlert(alerttitle: SERVER_ISSUE_TITLE, alertmessage: SERVER_ISSUE_MESSAGE, ButtonTitle: "Ok", viewController: self)
                    }
        }
    }
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
}

extension MenuControllerVC: UITableViewDelegate {
    
}
