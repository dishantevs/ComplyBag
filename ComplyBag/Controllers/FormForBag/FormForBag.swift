//
//  FormForBag.swift
//  ComplyBag
//
//  Created by Apple on 13/01/21.
//

import UIKit
import Alamofire
import SwiftyJSON

class FormForBag: UIViewController {

    // ***************************************************************** // nav
                    
        @IBOutlet weak var navigationBar:UIView! {
            didSet {
                navigationBar.backgroundColor = NAVIGATION_COLOR
            }
        }
            
        @IBOutlet weak var btnBack:UIButton! {
            didSet {
                btnBack.tintColor = NAVIGATION_BACK_COLOR
            }
        }
            
        @IBOutlet weak var lblNavigationTitle:UILabel! {
            didSet {
                lblNavigationTitle.text = NEW_BAG_PAGE_NAVIGATION_TITLE
                lblNavigationTitle.textColor = NAVIGATION_TITLE_COLOR
                lblNavigationTitle.backgroundColor = .clear
            }
        }
                    
    // ***************************************************************** // nav
    
    let cellReuseIdentifier = "formForBagTableCell"
    
    var mainBagIdIs:String!
    var selectedBagIdIs:String!
    var selectedIndex:Int!
    var arrGetFullListOfBags:NSMutableArray! = []
    
    var arrSavedValueAndSendToServer:NSMutableArray! = []
    
    var strFormatIs:String!
    var strBytesIs:String!
    var strOrientationIs:String!
    var strECLevelIs:String!
    
    // MARK:- TABLE VIEW -
    @IBOutlet weak var tbleView: UITableView! {
        didSet {
            self.tbleView.delegate = self
            self.tbleView.dataSource = self
            self.tbleView.backgroundColor = .white
            self.tbleView.tableFooterView = UIView.init(frame: CGRect(origin: .zero, size: .zero))
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.btnBack.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
        
        print(mainBagIdIs as Any)
        print(selectedBagIdIs as Any)
        print(selectedIndex as Any)
        print(arrGetFullListOfBags as Any)
        
        /*
         "date_harvest" = "";
         "destination_address" = "";
         "destination_entity" = "";
         "destination_number" = "";
         "ec_level" = null;
         "entity_phone" = "";
         "facility_location" = "";
         fomatId = "org.gs1.EAN-13";
         "manifest_number" = "";
         name = "";
         notes = "";
         "originating_entity" = "";
         "raw_bytes" = "\t";
         "room_number" = "";
         strain = "";
         tagName = 0036000291452;
         tagSaved = no;
         "tranported_lic" = "";
         "transporter_number" = "";
         "trasnporter_name" = "";
         */
        
        
        
        
        self.isDataSavedAlreadyOrNot()
        
        /*let paramsArray = self.arrGetFullListOfBags
        let paramsJSON = JSON(paramsArray as Any)
        let paramsString = paramsJSON.rawString(String.Encoding.utf8, options: JSONSerialization.WritingOptions.prettyPrinted)!
        print(paramsString as Any)*/
    }

    @objc func backClickMethod() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func isDataSavedAlreadyOrNot() {
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! FormForBagTableCell
        
        let item = arrGetFullListOfBags[selectedIndex] as? [String:Any]
        
        var my_string = (item!["raw_bytes"] as! String)
        my_string = my_string.replacingOccurrences(of: "Optional", with: "", options: NSString.CompareOptions.literal, range: nil)
        
        
        
        cell.lblTagId.text!         = "TAG ID : "+(item!["tagName"] as! String)
        cell.lblFormat.text!        = "Format : "+(item!["fomatId"] as! String)
        cell.lblRawByte.text!       = "Raw Bytes : "+my_string
        cell.lblOrientation.text!   = "Orientation : "+(item!["orientation"] as! String)
        cell.lblEClevel.text!       = "EC Level : "+(item!["ec_level"] as! String)
        
        self.strFormatIs = (item!["fomatId"] as! String)
        self.strBytesIs = my_string
        self.strOrientationIs = (item!["orientation"] as! String)
        self.strECLevelIs = (item!["ec_level"] as! String)
        
        if (item!["tagSaved"] as! String) == "yes" {
            
            cell.txtDateOfHarvest.text! = (item!["date_harvest"] as! String)
            cell.txtDestinationAddress.text! = (item!["destination_address"] as! String)
            cell.txtDestinationEntity.text! = (item!["destination_entity"] as! String)
            cell.txtDestinationPhoneNumber.text! = (item!["destination_number"] as! String)
            cell.txtOriginatingEntityPhoneNumber.text! = (item!["entity_phone"] as! String)
            cell.txtFacilityLocation.text! = (item!["facility_location"] as! String)
            cell.txtMenifestEntiry.text! = (item!["manifest_number"] as! String)
            cell.txtName.text! = (item!["name"] as! String)
            cell.txtNotes.text! = (item!["notes"] as! String)
            cell.txtOriginatingEntity.text! = (item!["originating_entity"] as! String)
            cell.txtRoomNumber.text! = (item!["room_number"] as! String)
            cell.txtStrain.text! = (item!["strain"] as! String)
            cell.txtTransporterLICnumber.text! = (item!["tranported_lic"] as! String)
            cell.txtTransporterPhoneNumber.text! = (item!["transporter_number"] as! String)
            cell.txtTransporterName.text! = (item!["trasnporter_name"] as! String)
            
        } else {
            
        }
        
    }
    
    
    
    /*
     [{"date_harvest":"01-12-2021",
     "destination_address":"m",
     "destination_entity":"b",
     "destination_number":"9",
     "entity_phone":"9",
     "facility_location":"g",
     "manifest_number":"m",
     "name":"m",
     "notes":"n",
     "originating_entity":"m",
     "room_number":"9",
     "strain":"m",
     "tranported_lic":"n",
     "transporter_number":"n",
     "trasnporter_name":"n"}]
     */
    
    @objc func savedDataInOwnCreateDictionary() {
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! FormForBagTableCell
        
        /*self.strFormatIs = (item!["fomatId"] as! String)
        self.strBytesIs = my_string
        self.strOrientationIs = (item!["orientation"] as! String)
        self.strECLevelIs = (item!["ec_level"] as! String)*/
        
        let myDictionary: [String:String] = [
            "date_harvest"          :String(cell.txtDateOfHarvest.text!),
            "destination_address"   :String(cell.txtDestinationAddress.text!),
            "destination_entity"    :String(cell.txtDestinationEntity.text!),
            "destination_number"    :String(cell.txtDestinationPhoneNumber.text!),
            "entity_phone"          :String(cell.txtOriginatingEntityPhoneNumber.text!),
            "facility_location"     :String(cell.txtFacilityLocation.text!),
            "manifest_number"       :String(cell.txtMenifestEntiry.text!),
            "name"                  :String(cell.txtName.text!),
            "notes"                 :String(cell.txtNotes.text!),
            "originating_entity"    :String(cell.txtOriginatingEntity.text!),
            "room_number"           :String(cell.txtRoomNumber.text!),
            "strain"                :String(cell.txtStrain.text!),
            "tranported_lic"        :String(cell.txtTransporterLICnumber.text!),
            "transporter_number"    :String(cell.txtTransporterPhoneNumber.text!),
            "trasnporter_name"      :String(cell.txtTransporterName.text!),
            "raw_bytes"             : String(self.strBytesIs),
            "ec_level"              : String(self.strECLevelIs),
            "fomatId"               : String(self.strFormatIs),
            "orientation"           : String(self.strOrientationIs)
        ]
        
        var res = [[String: String]]()
        res.append(myDictionary)
        
        self.arrSavedValueAndSendToServer.addObjects(from: res)
        
        self.addBagsWB()
    }
    
    @objc func addBagsWB() {
        
        self.view.endEditing(true)
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
        
        let paramsArray = self.arrSavedValueAndSendToServer
        let paramsJSON = JSON(paramsArray as Any)
        let paramsString = paramsJSON.rawString(String.Encoding.utf8, options: JSONSerialization.WritingOptions.prettyPrinted)!
        
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            
            let x : Int = person["userId"] as! Int
            let myString = String(x)
            
        let params = AddBags(action: "addscan",
                             userId: String(myString),
                             BAGID: String(mainBagIdIs),
                             TAG: String(selectedBagIdIs),
                             scanResponse: String(paramsString),
                             image: "")
        
        AF.request(APPLICATION_BASE_URL,
                   method: .post,
                   parameters: params,
                   encoder: JSONParameterEncoder.default).responseJSON { response in
                    // debugPrint(response.result)
                    
                    switch response.result {
                    case let .success(value):
                        
                        let JSON = value as! NSDictionary
                        print(JSON as Any)
                        
                        var strSuccess : String!
                        strSuccess = JSON["status"]as Any as? String
                       
                        if strSuccess == String("Success") {
                            print("yes")
                            
                            self.updateData()
                           
                        } else {
                            print("no")
                            ERProgressHud.sharedInstance.hide()
                            
                            var strSuccess2 : String!
                            strSuccess2 = JSON["msg"]as Any as? String
                            
                            Utils.showAlert(alerttitle: String(strSuccess), alertmessage: String(strSuccess2), ButtonTitle: "Ok", viewController: self)
                            
                        }
                        
                    case let .failure(error):
                        print(error)
                        ERProgressHud.sharedInstance.hide()
                        
                        Utils.showAlert(alerttitle: SERVER_ISSUE_TITLE, alertmessage: SERVER_ISSUE_MESSAGE, ButtonTitle: "Ok", viewController: self)
                    }
        }
    }
    }
    
    @objc func updateData() {
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! FormForBagTableCell
        
        print(self.arrGetFullListOfBags as Any)
        self.arrGetFullListOfBags.removeObject(at: selectedIndex)
        
        let myDictionary: [String:String] = [
            "tagSaved"              :String("yes"),
            "tagName"               :String(self.selectedBagIdIs),
            "date_harvest"          :String(cell.txtDateOfHarvest.text!),
            "destination_address"   :String(cell.txtDestinationAddress.text!),
            "destination_entity"    :String(cell.txtDestinationEntity.text!),
            "destination_number"    :String(cell.txtDestinationPhoneNumber.text!),
            "entity_phone"          :String(cell.txtOriginatingEntityPhoneNumber.text!),
            "facility_location"     :String(cell.txtFacilityLocation.text!),
            "manifest_number"       :String(cell.txtMenifestEntiry.text!),
            "name"                  :String(cell.txtName.text!),
            "notes"                 :String(cell.txtNotes.text!),
            "originating_entity"    :String(cell.txtOriginatingEntity.text!),
            "room_number"           :String(cell.txtRoomNumber.text!),
            "strain"                :String(cell.txtStrain.text!),
            "tranported_lic"        :String(cell.txtTransporterLICnumber.text!),
            "transporter_number"    :String(cell.txtTransporterPhoneNumber.text!),
            "trasnporter_name"      :String(cell.txtTransporterName.text!),
            "raw_bytes"             : String(self.strBytesIs),
            "ec_level"              : String(self.strECLevelIs),
            "fomatId"               : String(self.strFormatIs),
            "orientation"           : String(self.strOrientationIs)
        ]
        self.arrGetFullListOfBags.insert(myDictionary, at: selectedIndex)
        
        print(self.arrGetFullListOfBags as Any)
        
        let defaults = UserDefaults.standard
        defaults.set(self.arrGetFullListOfBags, forKey: "keySavedWhenDataSubmitSuccessfullyToOurServer")
        
        ERProgressHud.sharedInstance.hide()
        
        self.navigationController?.popViewController(animated: true)
    }
}

extension FormForBag: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cell:FormForBagTableCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! FormForBagTableCell
        
        let backgroundView = UIView()
        backgroundView.backgroundColor = .clear
        cell.selectedBackgroundView = backgroundView
        
        cell.backgroundColor = .clear
        
        cell.btnSubmit.addTarget(self, action: #selector(savedDataInOwnCreateDictionary), for: .touchUpInside)
        cell.btnDateOfHarvest.addTarget(self, action: #selector(dateOfHarvestClickMethod), for: .touchUpInside)
        
        return cell
        
    }
    
    @objc func dateOfHarvestClickMethod() {
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! FormForBagTableCell
        
        RPicker.selectDate(title: "Select Date", didSelectDate: {[weak self] (selectedDate) in
            // TODO: Your implementation for date
            // self?.outputLabel.text = selectedDate.dateString("MMM-dd-YYYY")
            cell.txtDateOfHarvest.text = selectedDate.dateString("dd/MMM/YYYY")
            self?.tbleView.reloadData()
        })
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView .deselectRow(at: indexPath, animated: true)
        
         
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 2000
    }
    
}

extension FormForBag: UITableViewDelegate {
    
}

 
