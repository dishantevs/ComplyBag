//
//  FormForBagTableCell.swift
//  ComplyBag
//
//  Created by Apple on 13/01/21.
//

import UIKit

class FormForBagTableCell: UITableViewCell {

    @IBOutlet weak var txtMenifestEntiry:UITextField! {
        didSet {
            txtMenifestEntiry.backgroundColor = .white
            let bottomLine = CALayer()
            bottomLine.frame = CGRect(x: 0.0, y: txtMenifestEntiry.frame.height - 1, width: txtMenifestEntiry.frame.width, height: 1.0)
            bottomLine.backgroundColor = UIColor.black.cgColor
            txtMenifestEntiry.borderStyle = UITextField.BorderStyle.none
            txtMenifestEntiry.layer.addSublayer(bottomLine)
            
            txtMenifestEntiry.textColor = .black
            txtMenifestEntiry.attributedPlaceholder = NSAttributedString(string: "menifest entiry...",
                                         attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
    }
    
    @IBOutlet weak var txtStrain:UITextField! {
        didSet {
            txtStrain.backgroundColor = .white
            let bottomLine = CALayer()
            bottomLine.frame = CGRect(x: 0.0, y: txtStrain.frame.height - 1, width: txtStrain.frame.width, height: 1.0)
            bottomLine.backgroundColor = UIColor.black.cgColor
            txtStrain.borderStyle = UITextField.BorderStyle.none
            txtStrain.layer.addSublayer(bottomLine)
            
            txtStrain.textColor = .black
            txtStrain.attributedPlaceholder = NSAttributedString(string: "strain...",
                                         attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
    }
    
    @IBOutlet weak var txtOriginatingEntity:UITextField! {
        didSet {
            txtOriginatingEntity.backgroundColor = .white
            let bottomLine = CALayer()
            bottomLine.frame = CGRect(x: 0.0, y: txtOriginatingEntity.frame.height - 1, width: txtOriginatingEntity.frame.width, height: 1.0)
            bottomLine.backgroundColor = UIColor.black.cgColor
            txtOriginatingEntity.borderStyle = UITextField.BorderStyle.none
            txtOriginatingEntity.layer.addSublayer(bottomLine)
            
            txtOriginatingEntity.textColor = .black
            txtOriginatingEntity.attributedPlaceholder = NSAttributedString(string: "originating phone number...",
                                         attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
    }
    
    @IBOutlet weak var txtOriginatingEntityPhoneNumber:UITextField! {
        didSet {
            txtOriginatingEntityPhoneNumber.backgroundColor = .white
            let bottomLine = CALayer()
            bottomLine.frame = CGRect(x: 0.0, y: txtOriginatingEntityPhoneNumber.frame.height - 1, width: txtOriginatingEntityPhoneNumber.frame.width, height: 1.0)
            bottomLine.backgroundColor = UIColor.black.cgColor
            txtOriginatingEntityPhoneNumber.borderStyle = UITextField.BorderStyle.none
            txtOriginatingEntityPhoneNumber.layer.addSublayer(bottomLine)
            
            txtOriginatingEntityPhoneNumber.textColor = .black
            txtOriginatingEntityPhoneNumber.attributedPlaceholder = NSAttributedString(string: "originating entity phone number...",
                                         attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
    }
    
    @IBOutlet weak var txtFacilityLocation:UITextField! {
        didSet {
            txtFacilityLocation.backgroundColor = .white
            let bottomLine = CALayer()
            bottomLine.frame = CGRect(x: 0.0, y: txtFacilityLocation.frame.height - 1, width: txtFacilityLocation.frame.width, height: 1.0)
            bottomLine.backgroundColor = UIColor.black.cgColor
            txtFacilityLocation.borderStyle = UITextField.BorderStyle.none
            txtFacilityLocation.layer.addSublayer(bottomLine)
            
            txtFacilityLocation.textColor = .black
            txtFacilityLocation.attributedPlaceholder = NSAttributedString(string: "facility location...",
                                         attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
    }
    
    @IBOutlet weak var txtRoomNumber:UITextField! {
        didSet {
            txtRoomNumber.backgroundColor = .white
            let bottomLine = CALayer()
            bottomLine.frame = CGRect(x: 0.0, y: txtRoomNumber.frame.height - 1, width: txtRoomNumber.frame.width, height: 1.0)
            bottomLine.backgroundColor = UIColor.black.cgColor
            txtRoomNumber.borderStyle = UITextField.BorderStyle.none
            txtRoomNumber.layer.addSublayer(bottomLine)
            
            txtRoomNumber.textColor = .black
            txtRoomNumber.attributedPlaceholder = NSAttributedString(string: "room number...",
                                         attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
    }
    
    @IBOutlet weak var txtDateOfHarvest:UITextField! {
        didSet {
            txtDateOfHarvest.backgroundColor = .white
            let bottomLine = CALayer()
            bottomLine.frame = CGRect(x: 0.0, y: txtDateOfHarvest.frame.height - 1, width: txtDateOfHarvest.frame.width, height: 1.0)
            bottomLine.backgroundColor = UIColor.black.cgColor
            txtDateOfHarvest.borderStyle = UITextField.BorderStyle.none
            txtDateOfHarvest.layer.addSublayer(bottomLine)
            
            txtDateOfHarvest.textColor = .black
            txtDateOfHarvest.attributedPlaceholder = NSAttributedString(string: "date of harvest...",
                                         attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
    }
    
    @IBOutlet weak var txtName:UITextField! {
        didSet {
            txtName.backgroundColor = .white
            let bottomLine = CALayer()
            bottomLine.frame = CGRect(x: 0.0, y: txtName.frame.height - 1, width: txtName.frame.width, height: 1.0)
            bottomLine.backgroundColor = UIColor.black.cgColor
            txtName.borderStyle = UITextField.BorderStyle.none
            txtName.layer.addSublayer(bottomLine)
            
            txtName.textColor = .black
            txtName.attributedPlaceholder = NSAttributedString(string: "name...",
                                         attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
    }
    
    @IBOutlet weak var btnDateOfHarvest:UIButton!
    
    @IBOutlet weak var txtDestinationEntity:UITextField! {
        didSet {
            txtDestinationEntity.backgroundColor = .white
            let bottomLine = CALayer()
            bottomLine.frame = CGRect(x: 0.0, y: txtDestinationEntity.frame.height - 1, width: txtDestinationEntity.frame.width, height: 1.0)
            bottomLine.backgroundColor = UIColor.black.cgColor
            txtDestinationEntity.borderStyle = UITextField.BorderStyle.none
            txtDestinationEntity.layer.addSublayer(bottomLine)
            
            txtDestinationEntity.textColor = .black
            txtDestinationEntity.attributedPlaceholder = NSAttributedString(string: "destination entity...",
                                         attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
    }
    
    @IBOutlet weak var txtDestinationAddress:UITextField! {
        didSet {
            txtDestinationAddress.backgroundColor = .white
            let bottomLine = CALayer()
            bottomLine.frame = CGRect(x: 0.0, y: txtDestinationAddress.frame.height - 1, width: txtDestinationAddress.frame.width, height: 1.0)
            bottomLine.backgroundColor = UIColor.black.cgColor
            txtDestinationAddress.borderStyle = UITextField.BorderStyle.none
            txtDestinationAddress.layer.addSublayer(bottomLine)
            
            txtDestinationAddress.textColor = .black
            txtDestinationAddress.attributedPlaceholder = NSAttributedString(string: "destination address...",
                                         attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
    }
    
    @IBOutlet weak var txtDestinationPhoneNumber:UITextField! {
        didSet {
            txtDestinationPhoneNumber.backgroundColor = .white
            let bottomLine = CALayer()
            bottomLine.frame = CGRect(x: 0.0, y: txtDestinationPhoneNumber.frame.height - 1, width: txtDestinationPhoneNumber.frame.width, height: 1.0)
            bottomLine.backgroundColor = UIColor.black.cgColor
            txtDestinationPhoneNumber.borderStyle = UITextField.BorderStyle.none
            txtDestinationPhoneNumber.layer.addSublayer(bottomLine)
            
            txtDestinationPhoneNumber.textColor = .black
            txtDestinationPhoneNumber.attributedPlaceholder = NSAttributedString(string: "destination phone number...",
                                         attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
    }
    
    @IBOutlet weak var txtTransporterName:UITextField! {
        didSet {
            txtTransporterName.backgroundColor = .white
            let bottomLine = CALayer()
            bottomLine.frame = CGRect(x: 0.0, y: txtTransporterName.frame.height - 1, width: txtTransporterName.frame.width, height: 1.0)
            bottomLine.backgroundColor = UIColor.black.cgColor
            txtTransporterName.borderStyle = UITextField.BorderStyle.none
            txtTransporterName.layer.addSublayer(bottomLine)
            
            txtTransporterName.textColor = .black
            txtTransporterName.attributedPlaceholder = NSAttributedString(string: "transporter name...",
                                         attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
    }
    
    @IBOutlet weak var txtTransporterLICnumber:UITextField! {
        didSet {
            txtTransporterLICnumber.backgroundColor = .white
            let bottomLine = CALayer()
            bottomLine.frame = CGRect(x: 0.0, y: txtTransporterLICnumber.frame.height - 1, width: txtTransporterLICnumber.frame.width, height: 1.0)
            bottomLine.backgroundColor = UIColor.black.cgColor
            txtTransporterLICnumber.borderStyle = UITextField.BorderStyle.none
            txtTransporterLICnumber.layer.addSublayer(bottomLine)
            
            txtTransporterLICnumber.textColor = .black
            txtTransporterLICnumber.attributedPlaceholder = NSAttributedString(string: "transporter lic number...",
                                         attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
    }
    
    @IBOutlet weak var txtTransporterPhoneNumber:UITextField! {
        didSet {
            txtTransporterPhoneNumber.backgroundColor = .white
            let bottomLine = CALayer()
            bottomLine.frame = CGRect(x: 0.0, y: txtTransporterPhoneNumber.frame.height - 1, width: txtTransporterPhoneNumber.frame.width, height: 1.0)
            bottomLine.backgroundColor = UIColor.black.cgColor
            txtTransporterPhoneNumber.borderStyle = UITextField.BorderStyle.none
            txtTransporterPhoneNumber.layer.addSublayer(bottomLine)
            
            txtTransporterPhoneNumber.textColor = .black
            txtTransporterPhoneNumber.attributedPlaceholder = NSAttributedString(string: "transporter phone number",
                                         attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
    }
    
    @IBOutlet weak var txtNotes:UITextView! {
        didSet {
            txtNotes.backgroundColor = .white
            let bottomLine = CALayer()
            bottomLine.frame = CGRect(x: 0.0, y: txtNotes.frame.height - 1, width: txtNotes.frame.width, height: 1.0)
            bottomLine.backgroundColor = UIColor.black.cgColor
            // txtNotes.borderStyle = UITextView.bord.none
            txtNotes.layer.addSublayer(bottomLine)
            txtNotes.text = ""
            txtNotes.textColor = .black
            
        }
    }
    
    @IBOutlet weak var btnSubmit:UIButton! {
        didSet {
            Dishu.buttonStyle(button: btnSubmit, bCornerRadius: 22, bBackgroundColor: UIColor(red: 54.0/255.0, green: 62.0/255.0, blue: 71.0/255.0, alpha: 1), bTitle: submitBagPage_TEXT_Submit, bTitleColor: .white)
        }
    }
    
    @IBOutlet weak var lblFormat:UILabel! {
        didSet {
            lblFormat.textColor = .black
        }
    }
    @IBOutlet weak var lblRawByte:UILabel! {
        didSet {
            lblRawByte.textColor = .black
        }
    }
    @IBOutlet weak var lblOrientation:UILabel! {
        didSet {
            lblOrientation.textColor = .black
        }
    }
    @IBOutlet weak var lblEClevel:UILabel! {
        didSet {
            lblEClevel.textColor = .black
        }
    }
    @IBOutlet weak var lblTagId:UILabel! {
        didSet {
            lblTagId.backgroundColor = .black
            lblTagId.textColor = .white
            lblTagId.textAlignment = .center
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
