//
//  NewBag.swift
//  ComplyBag
//
//  Created by Apple on 09/01/21.
//

import UIKit

import AVFoundation
import Alamofire

class NewBag: UIViewController , UITextFieldDelegate {

    // ***************************************************************** // nav
                    
        @IBOutlet weak var navigationBar:UIView! {
            didSet {
                navigationBar.backgroundColor = NAVIGATION_COLOR
            }
        }
            
        @IBOutlet weak var btnBack:UIButton! {
            didSet {
                btnBack.tintColor = NAVIGATION_BACK_COLOR
            }
        }
            
        @IBOutlet weak var lblNavigationTitle:UILabel! {
            didSet {
                lblNavigationTitle.text = NEW_BAG_PAGE_NAVIGATION_TITLE
                lblNavigationTitle.textColor = NAVIGATION_TITLE_COLOR
                lblNavigationTitle.backgroundColor = .clear
            }
        }
                    
    // ***************************************************************** // nav
    
    // var captureSession: AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!
    
    @IBOutlet weak var viewbg:UIView!
    
    
    
    
    
    @IBOutlet var messageLabel:UILabel!
    @IBOutlet var topbar: UIView!
    
    var captureSession = AVCaptureSession()
    
    var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    var qrCodeFrameView: UIView?

    private let supportedCodeTypes = [AVMetadataObject.ObjectType.upce,
                                      AVMetadataObject.ObjectType.code39,
                                      AVMetadataObject.ObjectType.code39Mod43,
                                      AVMetadataObject.ObjectType.code93,
                                      AVMetadataObject.ObjectType.code128,
                                      AVMetadataObject.ObjectType.ean8,
                                      AVMetadataObject.ObjectType.ean13,
                                      AVMetadataObject.ObjectType.aztec,
                                      AVMetadataObject.ObjectType.pdf417,
                                      AVMetadataObject.ObjectType.itf14,
                                      AVMetadataObject.ObjectType.dataMatrix,
                                      AVMetadataObject.ObjectType.interleaved2of5,
                                      AVMetadataObject.ObjectType.qr]
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        // self.btnBack.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
        
        self.btnBack.addTarget(self, action: #selector(backCLickMethod), for: .touchUpInside)
        
        self.newScannerInitialization()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    @objc func backCLickMethod() {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK:- NEW CODE -
    @objc func newScannerInitialization() {
        
        guard let captureDevice = AVCaptureDevice.default(for: AVMediaType.video) else {
            print("Failed to get the camera device")
            return
        }
        
        do {
            // Get an instance of the AVCaptureDeviceInput class using the previous device object.
            let input = try AVCaptureDeviceInput(device: captureDevice)
            
            // Set the input device on the capture session.
            captureSession.addInput(input)
            
            // Initialize a AVCaptureMetadataOutput object and set it as the output device to the capture session.
            let captureMetadataOutput = AVCaptureMetadataOutput()
            captureSession.addOutput(captureMetadataOutput)
            
            // Set delegate and use the default dispatch queue to execute the call back
            captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            captureMetadataOutput.metadataObjectTypes = supportedCodeTypes
//            captureMetadataOutput.metadataObjectTypes = [AVMetadataObject.ObjectType.qr]
            
        } catch {
            // If any error occurs, simply print it out and don't continue any more.
            print(error)
            return
        }
        
        // Initialize the video preview layer and add it as a sublayer to the viewPreview view's layer.
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        videoPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        videoPreviewLayer?.frame = view.layer.bounds
        view.layer.addSublayer(videoPreviewLayer!)
        
        // Start video capture.
        captureSession.startRunning()
        
        // Move the message label and top bar to the front
        view.bringSubviewToFront(messageLabel)
        view.bringSubviewToFront(topbar)
        
        // Initialize QR Code Frame to highlight the QR code
        qrCodeFrameView = UIView()
        
        if let qrCodeFrameView = qrCodeFrameView {
            qrCodeFrameView.layer.borderColor = UIColor.green.cgColor
            qrCodeFrameView.layer.borderWidth = 2
            view.addSubview(qrCodeFrameView)
            view.bringSubviewToFront(qrCodeFrameView)
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Helper methods -

    func launchApp(decodedURL: String) {
        
        if presentedViewController != nil {
            return
        }
        
        let alertPrompt = UIAlertController(title: nil, message: "Code Number : \(decodedURL)", preferredStyle: .actionSheet)
        let confirmAction = UIAlertAction(title: "Confirm", style: UIAlertAction.Style.default, handler: { (action) -> Void in
            
            if let url = URL(string: decodedURL) {
                if UIApplication.shared.canOpenURL(url) {
                    UIApplication.shared.open(url)
                    
                    print("i am url")
                } else {
                    print("i am not url 2")
                    
                    self.checkThisCodeIsExistOrNot(strCode: "\(decodedURL)")
                }
            } else {
                print("i am not url")
            }
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil)
        
        alertPrompt.addAction(confirmAction)
        alertPrompt.addAction(cancelAction)
        
        present(alertPrompt, animated: true, completion: nil)
    }
    
    private func updatePreviewLayer(layer: AVCaptureConnection, orientation: AVCaptureVideoOrientation) {
        layer.videoOrientation = orientation
        videoPreviewLayer?.frame = self.view.bounds
    }
  
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    
        if let connection =  self.videoPreviewLayer?.connection  {
            let currentDevice: UIDevice = UIDevice.current
            let orientation: UIDeviceOrientation = currentDevice.orientation
            let previewLayerConnection : AVCaptureConnection = connection
      
            if previewLayerConnection.isVideoOrientationSupported {
                switch (orientation) {
                case .portrait:
                    updatePreviewLayer(layer: previewLayerConnection, orientation: .portrait)
                    break
                case .landscapeRight:
                    updatePreviewLayer(layer: previewLayerConnection, orientation: .landscapeLeft)
                    break
                case .landscapeLeft:
                    updatePreviewLayer(layer: previewLayerConnection, orientation: .landscapeRight)
                    break
                case .portraitUpsideDown:
                    updatePreviewLayer(layer: previewLayerConnection, orientation: .portraitUpsideDown)
                    break
                default:
                    updatePreviewLayer(layer: previewLayerConnection, orientation: .portrait)
                    break
                }
            }
        }
  }
    
    
    
    
    // MARK:- CHECK THIS CODE EXIST OR NOT IN DB -
    @objc func checkThisCodeIsExistOrNot(strCode:String) {
        
        self.view.endEditing(true)
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
        
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
         // let str:String = person["role"] as! String
        
            let x : Int = person["userId"] as! Int
            let myString = String(x)
            
            let params = CheckBagExistWB(action: "checkbag",
                                         userId: String(myString),
                                         BAGID: String(strCode))
        
            print(params as Any)
            
            AF.request(APPLICATION_BASE_URL,
                       method: .post,
                       parameters: params,
                       encoder: JSONParameterEncoder.default).responseJSON { response in
                    // debugPrint(response.result)
                    
                        switch response.result {
                        case let .success(value):
                        
                            let JSON = value as! NSDictionary
                            print(JSON as Any)
                        
                            var strSuccess : String!
                            strSuccess = JSON["status"]as Any as? String
                       
                            if strSuccess == String("Success") {
                                print("yes")
                                ERProgressHud.sharedInstance.hide()
                           
                                self.captureSession.stopRunning()
                                self.pushToNewBagAfterSearch(strbarCodeKey: strCode)
                              
                            } else {
                                print("no")
                                ERProgressHud.sharedInstance.hide()
                            
                                var strSuccess2 : String!
                                strSuccess2 = JSON["msg"]as Any as? String
                            
                                
                                
                                if String(myString) == "\(JSON["userId"] as! Int)" {
                                    
                                    let alert = UIAlertController(title: "Alert", message: strSuccess2,
                                                              preferredStyle: UIAlertController.Style.alert)

                                    alert.addAction(UIAlertAction(title: "Edit Details",
                                                                  style: UIAlertAction.Style.default,
                                                                  handler: {(_: UIAlertAction!) in
                                                                
                                                                self.captureSession.startRunning()
                                                                
                                                                    let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "FormBagSubDetailsId") as? FormBagSubDetails
                                                                    
                                                                    push!.disableTextFieldsOrNot = "no"
                                                                    push!.mainBagIdIs = "\(JSON["bagId"] as! String)"
                                                                    push!.getScanId = "\(JSON["scanId"] as! Int)"
                                                                    push!.getUserIdId = "\(JSON["userId"] as! Int)"
                                                                    
                                                                    self.navigationController?.pushViewController(push!, animated: true)
                                                                    
                                                                  }))
                                    
                                    alert.addAction(UIAlertAction(title: "Dismiss",
                                                                  style: .destructive,
                                                                  handler: {(_: UIAlertAction!) in
                                                                
                                                                    self.captureSession.startRunning()
                                                                
                                                                  }))
                                    
                                    self.present(alert, animated: true, completion: nil)
                                    
                                } else {
                                    
                                    let alert = UIAlertController(title: "Alert", message: strSuccess2,
                                                              preferredStyle: UIAlertController.Style.alert)

                                    alert.addAction(UIAlertAction(title: "View Details",
                                                                  style: .default,
                                                                  handler: {(_: UIAlertAction!) in
                                                                
                                                                self.captureSession.startRunning()
                                                                
                                                                    let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "FormBagSubDetailsId") as? FormBagSubDetails
                                                                    
                                                                    push!.disableTextFieldsOrNot = "yes"
                                                                    push!.mainBagIdIs = "\(JSON["bagId"] as! String)"
                                                                    push!.getScanId = "\(JSON["scanId"] as! Int)"
                                                                    push!.getUserIdId = "\(JSON["userId"] as! Int)"
                                                                    
                                                                    self.navigationController?.pushViewController(push!, animated: true)
                                                                    
                                                                    
                                                                  }))
                                    
                                    alert.addAction(UIAlertAction(title: "Dismiss",
                                                                  style: .destructive,
                                                                  handler: {(_: UIAlertAction!) in
                                                                
                                                                    self.captureSession.startRunning()
                                                                
                                                                  }))
                                    
                                    self.present(alert, animated: true, completion: nil)
                                    
                                    
                                }
                                
                            
                            }
                        
                        case let .failure(error):
                            print(error)
                            ERProgressHud.sharedInstance.hide()
                        
                            Utils.showAlert(alerttitle: SERVER_ISSUE_TITLE, alertmessage: SERVER_ISSUE_MESSAGE, ButtonTitle: "Ok", viewController: self)
                        }
                       }
        }
        
    }
    
    @objc func pushToNewBagAfterSearch(strbarCodeKey:String) {
        let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "NewBagAfterSearchId") as? NewBagAfterSearch
        push!.strMainBagId = strbarCodeKey
        self.navigationController?.pushViewController(push!, animated: true)
    }
    
    
    
}


extension NewBag: AVCaptureMetadataOutputObjectsDelegate {
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        // Check if the metadataObjects array is not nil and it contains at least one object.
        if metadataObjects.count == 0 {
            qrCodeFrameView?.frame = CGRect.zero
            messageLabel.text = "No QR code is detected"
            return
        }
        
        // Get the metadata object.
        let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        
        if supportedCodeTypes.contains(metadataObj.type) {
            // If the found metadata is equal to the QR code metadata (or barcode) then update the status label's text and set the bounds
            let barCodeObject = videoPreviewLayer?.transformedMetadataObject(for: metadataObj)
            qrCodeFrameView?.frame = barCodeObject!.bounds
            
            if metadataObj.stringValue != nil {
                launchApp(decodedURL: metadataObj.stringValue!)
                messageLabel.text = metadataObj.stringValue
            }
        }
    }
    
}
