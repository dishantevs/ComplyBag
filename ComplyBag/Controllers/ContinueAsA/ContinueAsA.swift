//
//  ContinueAsA.swift
//  ComplyBag
//
//  Created by Apple on 09/01/21.
//

import UIKit
 
class ContinueAsA: UIViewController {

    // ***************************************************************** // nav
        
        @IBOutlet weak var navigationBar:UIView! {
            didSet {
                navigationBar.backgroundColor = NAVIGATION_COLOR
            }
        }
        @IBOutlet weak var btnBack:UIButton! {
            didSet {
                btnBack.tintColor = NAVIGATION_BACK_COLOR
            }
        }
        @IBOutlet weak var lblNavigationTitle:UILabel! {
            didSet {
                lblNavigationTitle.text = "LOGIN"
                lblNavigationTitle.textColor = NAVIGATION_TITLE_COLOR
            }
        }
        
    // ***************************************************************** // nav
    
    @IBOutlet weak var btnSignIn:UIButton! {
        didSet {
            Dishu.buttonStyle(button: btnSignIn, bCornerRadius: 22, bBackgroundColor: BUTTON_BLUE_COLOR, bTitle: continueAsa_TEXT_SignIn, bTitleColor: .white)
        }
    }
    
    @IBOutlet weak var btnCreateAnAccount:UIButton! {
        didSet {
            Dishu.buttonStyle(button: btnCreateAnAccount, bCornerRadius: 22, bBackgroundColor: .white, bTitle: continueAsa_TEXT_CreateAnAccount, bTitleColor: .black)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        self.btnSignIn.addTarget(self, action: #selector(signInClickMethod), for: .touchUpInside)
        self.btnCreateAnAccount.addTarget(self, action: #selector(createAnAccountClickMethod), for: .touchUpInside)
        
        self.rememberMe()
    }
    
    @objc func rememberMe() {
        // MARK:- PUSH -
        
        let defaults = UserDefaults.standard
        if let myString = defaults.string(forKey: "rememberMefoReal") {
            if myString == "1" {
                
                // MARK:- PUSH -
                if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
                    print(person as Any)
                        
                    let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "DashboardId")
                    self.navigationController?.pushViewController(push, animated: false)
                    
                } else {
                    
                }
                
            } else {
                
            }
        }
        
        /*if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            print(person as Any)
            
            //
            
            // let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "FormForBagId")
            // self.navigationController?.pushViewController(push, animated: false)
            
                // Member
            let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "DashboardId")
            self.navigationController?.pushViewController(push, animated: false)
            
        }*/
    }
    
    @objc func signInClickMethod() {
        let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "LoginId")
        self.navigationController?.pushViewController(push, animated: true)
    }
    
    @objc func createAnAccountClickMethod() {
        let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "RegisterId")
        self.navigationController?.pushViewController(push, animated: true)
    }
    
}
