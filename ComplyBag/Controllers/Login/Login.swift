//
//  Login.swift
//  ShootWorthy
//
//  Created by Apple on 19/12/20.
//

import UIKit
import Alamofire

class Login: UIViewController, UITextFieldDelegate {

    // ***************************************************************** // nav
                    
        @IBOutlet weak var navigationBar:UIView! {
            didSet {
                navigationBar.backgroundColor = NAVIGATION_COLOR
            }
        }
            
        @IBOutlet weak var btnBack:UIButton! {
            didSet {
                btnBack.tintColor = NAVIGATION_BACK_COLOR
            }
        }
            
        @IBOutlet weak var lblNavigationTitle:UILabel! {
            didSet {
                lblNavigationTitle.text = LOGIN_PAGE_NAVIGATION_TITLE
                lblNavigationTitle.textColor = NAVIGATION_TITLE_COLOR
                lblNavigationTitle.backgroundColor = .clear
            }
        }
                    
    // ***************************************************************** // nav
    
    var whoIam:String!
    
    @IBOutlet weak var viewLoginBG:UIView! {
        didSet {
            viewLoginBG.layer.cornerRadius = 12
            viewLoginBG.clipsToBounds = true
            viewLoginBG.backgroundColor = .white //UIColor.init(red: 252.0/255.0, green: 252.0/255.0, blue: 252.0/255.0, alpha: 1)
            
            // border
            viewLoginBG.layer.borderColor = UIColor.lightGray.cgColor
            viewLoginBG.layer.borderWidth = 1.0

            // drop shadow
            viewLoginBG.layer.shadowColor = UIColor.black.cgColor
            viewLoginBG.layer.shadowOpacity = 0.8
            viewLoginBG.layer.shadowRadius = 3.0
            viewLoginBG.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
            
        }
    }
    
    @IBOutlet weak var btnDontHaveAnAccount:UIButton! {
        didSet {
             Dishu.buttonStyle(button: btnDontHaveAnAccount, bCornerRadius: 22, bBackgroundColor: .clear, bTitle: loginPage_TEXT_DontHaveAnAccount, bTitleColor: NAVIGATION_COLOR)
            
            
        }
    }
    
    @IBOutlet weak var btnForgotPassword:UIButton! {
        didSet {
            Dishu.buttonStyle(button: btnForgotPassword, bCornerRadius: 22, bBackgroundColor: .clear, bTitle: loginPage_TEXT_ForgotPassword, bTitleColor: NAVIGATION_COLOR)
        }
    }
    
    @IBOutlet weak var btnSignIn:UIButton! {
        didSet {
            Dishu.buttonStyle(button: btnSignIn, bCornerRadius: 22, bBackgroundColor: UIColor(red: 54.0/255.0, green: 62.0/255.0, blue: 71.0/255.0, alpha: 1), bTitle: loginPage_TEXT_SignIn, bTitleColor: .white)
        }
    }
    
    @IBOutlet weak var btnCheckUncheck:UIButton!
    
    @IBOutlet weak var lblEmailTitle:UILabel! {
        didSet {
            lblEmailTitle.textColor = UIColor.init(red: 185.0/255.0, green: 185.0/255.0, blue: 185.0/255.0, alpha: 1)
        }
    }
    @IBOutlet weak var txtEmail:UITextField! {
        didSet {
            txtEmail.textColor = .black
            txtEmail.attributedPlaceholder = NSAttributedString(string: "email",
                                         attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
    }
    @IBOutlet weak var txtPassword:UITextField! {
        didSet {
            txtPassword.textColor = .black
            txtPassword.attributedPlaceholder = NSAttributedString(string: "password",
                                         attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
            txtPassword.isSecureTextEntry = true
        }
    }
    
    @IBOutlet weak var lblLoginMessage:UILabel! {
        didSet {
            lblLoginMessage.text = "Hello\nSign into your Account."
        }
    }
    
    @IBOutlet weak var lblRememberMe:UILabel! {
        didSet {
            lblRememberMe.textColor = NAVIGATION_COLOR
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.txtEmail.delegate = self
        self.txtPassword.delegate = self
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        self.view.backgroundColor = UIColor.init(red: 252.0/255.0, green: 252.0/255.0, blue: 252.0/255.0, alpha: 1)
        
        // MARK:- VIEW UP WHEN CLICK ON TEXT FIELD -
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        let defaults = UserDefaults.standard
        defaults.set("0", forKey: "rememberMefoReal")
        
        self.btnCheckUncheck.tintColor = NAVIGATION_COLOR
        self.btnCheckUncheck.tag = 0
        self.btnCheckUncheck.setImage(UIImage(systemName: "checkmark.circle"), for: .normal)
        self.btnCheckUncheck.addTarget(self, action: #selector(checkUncheckClickMethod), for: .touchUpInside)
        
        self.btnSignIn.addTarget(self, action: #selector(validationBeforeLogin), for: .touchUpInside)
        self.btnDontHaveAnAccount.addTarget(self, action: #selector(signUpClickMethod), for: .touchUpInside)
        self.btnBack.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
    
        self.btnForgotPassword.addTarget(self, action: #selector(forgotPasswordClickMethod), for: .touchUpInside)
     }
    
    @objc func checkUncheckClickMethod() {
        
        if self.btnCheckUncheck.tag == 0 { // yes
            
            let defaults = UserDefaults.standard
            defaults.set("1", forKey: "rememberMefoReal")
            
            self.btnCheckUncheck.setImage(UIImage(systemName: "checkmark.circle.fill"), for: .normal)
            
            self.btnCheckUncheck.tag = 1
            
        } else { // no
            
            let defaults = UserDefaults.standard
            defaults.set("0", forKey: "rememberMefoReal")
            
            
            self.btnCheckUncheck.setImage(UIImage(systemName: "checkmark.circle"), for: .normal)
            
            self.btnCheckUncheck.tag = 0
            
        }
        
    }
    
    @objc func backClickMethod() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }

    @objc func forgotPasswordClickMethod() {
        let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ForgotPasswordId") as? ForgotPassword
        self.navigationController?.pushViewController(push!, animated: true)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    // MARK:- KEYBOARD WILL SHOW -
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }

    // MARK:- KEYBOARD WILL HIDE -
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    @objc func signUpClickMethod() {
        let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "RegisterId")
        self.navigationController?.pushViewController(push, animated: true)
    }
    
    @objc func termsAofUseClickMethod() {
        // let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "TermsOfUseId") as? TermsOfUse
        // self.navigationController?.pushViewController(push!, animated: true)
    }
    
    @objc func signInAs() {
        // whoIam
    }
    
     
    // MARK:- VALIDATION BEFORE REGISTRATION -
    @objc func validationBeforeLogin() {
    
        
        
        if String(txtEmail.text!) == "" {
            
            self.validationAlertPopup(strAlertTitle: "Alert", strMessage: "Email Address ", strMessageAlert: "should not be Empty")
            
        } else if !isValidEmail(testStr: txtEmail.text!) {
            
            print("Validate EmailID")
            let alert = UIAlertController(title: "Alert!", message: "Please Enter a valid E-Mail Address.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
            self.present(alert, animated: true)
            
        } else if String(txtPassword.text!) == "" {
            
            self.validationAlertPopup(strAlertTitle: "Alert", strMessage: "Password ", strMessageAlert: "should not be Empty")
            
        } else {
            
            self.loginWb()
        }
    }
    
    @objc func validationAlertPopup(strAlertTitle:String,strMessage:String,strMessageAlert:String) {
    
        let alert = UIAlertController(title: String(strAlertTitle), message: String(strMessage)+String(strMessageAlert), preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
         }))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    @objc func loginWb() {
        
        self.view.endEditing(true)
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
        
      
        let forgotPasswordP = LoginParam(action: "login",
                                      email: String(txtEmail.text!),
                                      password: String(txtPassword.text!))
        
        AF.request(APPLICATION_BASE_URL,
                   method: .post,
                   parameters: forgotPasswordP,
                   encoder: JSONParameterEncoder.default).responseJSON { response in
                    // debugPrint(response.result)
                    
                    switch response.result {
                    case let .success(value):
                        
                        let JSON = value as! NSDictionary
                        print(JSON as Any)
                        
                        var strSuccess : String!
                        strSuccess = JSON["status"]as Any as? String
                        
                        // var strSuccess2 : String!
                        // strSuccess2 = JSON["msg"]as Any as? String
                        
                        /*
                         
                         */
                        
                        if strSuccess == String("success") {
                            print("yes")
                            ERProgressHud.sharedInstance.hide()
                           
                            var dict: Dictionary<AnyHashable, Any>
                            dict = JSON["data"] as! Dictionary<AnyHashable, Any>
                            
                             let defaults = UserDefaults.standard
                             defaults.setValue(dict, forKey: "keyLoginFullData")
                            
                            
                            /*// MARK:- PUSH -
                            if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
                            
                                if person["role"] as! String == "Member" {
                                
                                    self.performSegue(withIdentifier: "afterLogin", sender: self)
                                    
                                } else {
                                    
                                }
                            }*/
                            
                            let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "DashboardId")
                            self.navigationController?.pushViewController(push, animated: true)
                            
 
                        } else {
                            print("no")
                            ERProgressHud.sharedInstance.hide()
                            
                            var strSuccess2 : String!
                            strSuccess2 = JSON["msg"]as Any as? String
                            
                            let alert = UIAlertController(title: "Error", message: strSuccess2,
                                                          preferredStyle: UIAlertController.Style.alert)

                            alert.addAction(UIAlertAction(title: "Ok",
                                                          style: UIAlertAction.Style.default,
                                                          handler: {(_: UIAlertAction!) in
                                                            //Sign out action
                                                            
                            }))
                            self.present(alert, animated: true, completion: nil)
                            
                        }
                        
                    case let .failure(error):
                        print(error)
                        ERProgressHud.sharedInstance.hide()
                        
                        Utils.showAlert(alerttitle: SERVER_ISSUE_TITLE, alertmessage: SERVER_ISSUE_MESSAGE, ButtonTitle: "Ok", viewController: self)
                    }
        }
        
    }
    
    func isValidEmail(testStr:String) -> Bool {
        print("validate emilId: \(testStr)")
        let emailRegEx = "^(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?(?:(?:(?:[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+(?:\\.[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+)*)|(?:\"(?:(?:(?:(?: )*(?:(?:[!#-Z^-~]|\\[|\\])|(?:\\\\(?:\\t|[ -~]))))+(?: )*)|(?: )+)\"))(?:@)(?:(?:(?:[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)(?:\\.[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)*)|(?:\\[(?:(?:(?:(?:(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))\\.){3}(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))))|(?:(?:(?: )*[!-Z^-~])*(?: )*)|(?:[Vv][0-9A-Fa-f]+\\.[-A-Za-z0-9._~!$&'()*+,;=:]+))\\])))(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: testStr)
        return result
    }
    
}
