
//
//  ForgotPassword.swift
//  Swipe
//
//  Created by evs_SSD on 2/5/20.
//  Copyright © 2020 Apple . All rights reserved.
//

import UIKit
import Alamofire
// import CRNotifications

class ForgotPassword: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var navigationBar:UIView! {
        didSet {
             navigationBar.backgroundColor = NAVIGATION_COLOR
        }
    }
    
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "FORGOT PASSWORD"
            lblNavigationTitle.textColor = .white
        }
    }
    
    @IBOutlet weak var btnBack:UIButton! {
        didSet {
            btnBack.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
        }
    }
    
    @IBOutlet weak var btnForgotPassword:UIButton! {
        didSet {
            Dishu.buttonStyle(button: btnForgotPassword, bCornerRadius: 22, bBackgroundColor: UIColor(red: 54.0/255.0, green: 62.0/255.0, blue: 71.0/255.0, alpha: 1), bTitle: "Submit", bTitleColor: .white)
        }
    }
    
    @IBOutlet weak var txtEmail:UITextField! {
        didSet {
            txtEmail.textColor = .black
            txtEmail.attributedPlaceholder = NSAttributedString(string: "email",
                                         attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        txtEmail.delegate = self
        self.btnForgotPassword.addTarget(self, action: #selector(forgot), for: .touchUpInside)
        self.btnBack.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
        
        self.view.backgroundColor = .white
    }
    
    @objc func backClickMethod() {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
          return .lightContent
    }
    
    /*
     "action"         : "forgotpassword",
     "email"         : String(txtEmail.text!)
     */
    @objc func forgot() {
        
        if txtEmail.text == "" {
            
            let alert = UIAlertController(title: String("Alert"), message: String("Email should not be empty."), preferredStyle: UIAlertController.Style.alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
             }))
            self.present(alert, animated: true, completion: nil)
            
        } else if !isValidEmail(testStr: txtEmail.text!) {
            
            print("Validate EmailID")
            let alert = UIAlertController(title: "Alert!", message: "Please Enter a valid E-Mail Address.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
            self.present(alert, animated: true)
            
        }
        else {
            
            self.view.endEditing(true)
            ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
            
          
            let forgotPasswordP = ForgotPasswordWB(action: "forgotpassword",
                                          email: String(txtEmail.text!))
            
            AF.request(APPLICATION_BASE_URL,
                       method: .post,
                       parameters: forgotPasswordP,
                       encoder: JSONParameterEncoder.default).responseJSON { response in
                        // debugPrint(response.result)
                        
                        switch response.result {
                        case let .success(value):
                            
                            let JSON = value as! NSDictionary
                            print(JSON as Any)
                            
                            var strSuccess : String!
                            strSuccess = JSON["status"]as Any as? String
                            
                             var strSuccess2 : String!
                             strSuccess2 = JSON["msg"]as Any as? String
                            
                            /*
                             
                             */
                            
                            if strSuccess == String("success") {
                                print("yes")
                                ERProgressHud.sharedInstance.hide()
                               
                                
                                let alert = UIAlertController(title: "Success", message: strSuccess2,
                                                              preferredStyle: UIAlertController.Style.alert)

                                alert.addAction(UIAlertAction(title: "Ok",
                                                              style: UIAlertAction.Style.default,
                                                              handler: {(_: UIAlertAction!) in
                                                                //Sign out action
                                                                self.backClickMethod()
                                }))
                                self.present(alert, animated: true, completion: nil)
     
                            } else {
                                print("no")
                                ERProgressHud.sharedInstance.hide()
                                
                                var strSuccess2 : String!
                                strSuccess2 = JSON["msg"]as Any as? String
                                
                                Utils.showAlert(alerttitle: String(strSuccess), alertmessage: String(strSuccess2), ButtonTitle: "Ok", viewController: self)
                                
                            }
                            
                        case let .failure(error):
                            print(error)
                            ERProgressHud.sharedInstance.hide()
                            
                            Utils.showAlert(alerttitle: SERVER_ISSUE_TITLE, alertmessage: SERVER_ISSUE_MESSAGE, ButtonTitle: "Ok", viewController: self)
                        }
            }
            
        }
       }
    
    
    
    func isValidEmail(testStr:String) -> Bool {
        print("validate emilId: \(testStr)")
        let emailRegEx = "^(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?(?:(?:(?:[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+(?:\\.[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+)*)|(?:\"(?:(?:(?:(?: )*(?:(?:[!#-Z^-~]|\\[|\\])|(?:\\\\(?:\\t|[ -~]))))+(?: )*)|(?: )+)\"))(?:@)(?:(?:(?:[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)(?:\\.[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)*)|(?:\\[(?:(?:(?:(?:(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))\\.){3}(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))))|(?:(?:(?: )*[!-Z^-~])*(?: )*)|(?:[Vv][0-9A-Fa-f]+\\.[-A-Za-z0-9._~!$&'()*+,;=:]+))\\])))(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: testStr)
        return result
    }
    
    
}
