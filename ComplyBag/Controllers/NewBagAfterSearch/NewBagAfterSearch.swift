//
//  NewBagAfterSearch.swift
//  ComplyBag
//
//  Created by Apple on 09/01/21.
//

import UIKit

import AVFoundation
import Alamofire

class NewBagAfterSearch: UIViewController, AVCaptureMetadataOutputObjectsDelegate {

    // ***************************************************************** // nav
                    
        @IBOutlet weak var navigationBar:UIView! {
            didSet {
                navigationBar.backgroundColor = NAVIGATION_COLOR
            }
        }
            
        @IBOutlet weak var btnBack:UIButton! {
            didSet {
                btnBack.tintColor = NAVIGATION_BACK_COLOR
            }
        }
            
        @IBOutlet weak var lblNavigationTitle:UILabel! {
            didSet {
                lblNavigationTitle.text = NEW_BAG_PAGE_NAVIGATION_TITLE
                lblNavigationTitle.textColor = NAVIGATION_TITLE_COLOR
                lblNavigationTitle.backgroundColor = .clear
            }
        }
                    
    // ***************************************************************** // nav
    
    let cellReuseIdentifier = "newBagAfterSearchTableCell"
    
    // var captureSession: AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!
        
    var scannedDataIs:Data!
    
    
    var checkThisTagIsScannedLocally:String!
    
    //#2
    @IBOutlet var messageLabel:UILabel!
    @IBOutlet var topbar: UIView!
    
    var captureSession = AVCaptureSession()
    
    var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    var qrCodeFrameView: UIView?

    private let supportedCodeTypes = [AVMetadataObject.ObjectType.upce,
                                      AVMetadataObject.ObjectType.code39,
                                      AVMetadataObject.ObjectType.code39Mod43,
                                      AVMetadataObject.ObjectType.code93,
                                      AVMetadataObject.ObjectType.code128,
                                      AVMetadataObject.ObjectType.ean8,
                                      AVMetadataObject.ObjectType.ean13,
                                      AVMetadataObject.ObjectType.aztec,
                                      AVMetadataObject.ObjectType.pdf417,
                                      AVMetadataObject.ObjectType.itf14,
                                      AVMetadataObject.ObjectType.dataMatrix,
                                      AVMetadataObject.ObjectType.interleaved2of5,
                                      AVMetadataObject.ObjectType.qr]
    
    
    
    
    
    
    
    
    
    @IBOutlet weak var imgBarCodePlaceholder:UIImageView! {
        didSet {
            imgBarCodePlaceholder.backgroundColor = .white
        }
    }
    
    @IBOutlet weak var viewbg:UIView! {
        didSet {
            viewbg.backgroundColor = .white
        }
    }
    
 
    var strMainBagId:String!
    var arrTotalBarCode:NSMutableArray! = []
    
    var arrListOfBarCodeUpload:NSMutableArray! = []
    
    var arrListForMatchData:NSMutableArray! = []
    
    var deviceOrientation:String!
    
    
    
    
    
    var getCodeName:String!
    var getCodeBytes:String!
    var getCodeType:String!
    
    
    // MARK:- TABLE VIEW -
    @IBOutlet weak var tbleView: UITableView! {
        didSet {
            self.tbleView.delegate = self
            self.tbleView.dataSource = self
            self.tbleView.backgroundColor = .white
            self.tbleView.tableFooterView = UIView.init(frame: CGRect(origin: .zero, size: .zero))
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.view.backgroundColor = .white
        
        self.viewbg.isHidden = true
        
        print(self.strMainBagId as Any)
        
        self.btnBack.addTarget(self, action: #selector(backCLickMethod), for: .touchUpInside)
        
        self.deviceOrientation = "unknown"
        
        NotificationCenter.default.addObserver(self, selector: #selector(NewBagAfterSearch.rotated), name: UIDevice.orientationDidChangeNotification, object: nil)
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        imgBarCodePlaceholder.isUserInteractionEnabled = true
        imgBarCodePlaceholder.addGestureRecognizer(tapGestureRecognizer)
    }
    
    deinit {
       NotificationCenter.default.removeObserver(self, name: UIDevice.orientationDidChangeNotification, object: nil)
    }

    @objc func rotated() {
        if UIDevice.current.orientation.isLandscape {
            print("Landscape")
            self.deviceOrientation = "Landscape"
        } else {
            print("Portrait")
            self.deviceOrientation = "Portrait"
        }
    }
    
    @objc func backCLickMethod() {
        for controller in self.navigationController!.viewControllers as Array {
        if controller.isKind(of: Dashboard.self) {
            self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
        // self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
         let defaults = UserDefaults.standard
        // let myarray = defaults.stringArray(forKey: "SavedStringArray") ?? [String]()
        if let person = defaults.stringArray(forKey: "keySavedWhenDataSubmitSuccessfullyToOurServer2") {
            print(person as Any)
        } else {
            print("no called")
        }
        
        let myarray = defaults.stringArray(forKey: "keySavedWhenDataSubmitSuccessfullyToOurServer") ?? [String]()
        print(myarray as Any)
        
        
        if let scannerCode = defaults.string(forKey: "keySaveSubScanBagCategory") {
            
            
            
            print("defaults savedString: \(scannerCode)")
            
            self.getCodeName = "\(scannerCode)"
            
            /*defaults.set("\(string)", forKey: "keySaveSubScanBagCategory")
            defaults.set("\(rawBytes)", forKey: "keySaveSubScanBagCategoryBytes")
            defaults.set("\(metadataObject.type.rawValue)", forKey: "keySaveSubScanBagCategoryType")*/
            
            if let scannerBytesIs = defaults.string(forKey: "keySaveSubScanBagCategoryBytes") {
                self.getCodeBytes = "\(scannerBytesIs)"
            }
            
            if let scannerType = defaults.string(forKey: "keySaveSubScanBagCategoryType") {
                self.getCodeType = "\(scannerType)"
            }
            
             
            self.checkThisTagIsScannedLocally = "no"
            
            self.isThisBagAlreadyExist(code: String(self.getCodeName), strRawBytes: String(self.getCodeBytes), strTypeOrId: String(self.getCodeType))
            
            
            
            
        } else {
            
        }
        
        
        print(self.arrTotalBarCode as Any)
        self.tbleView.delegate = self
        self.tbleView.dataSource = self
        self.tbleView.reloadData()
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        // let tappedImage = tapGestureRecognizer.view as! UIImageView

        // self.imgBarCodePlaceholder.isHidden = true
        // self.viewbg.isHidden = false
        
        // self.captureBarcode()
        
        // self.newScannerInitialization()
        
        
        let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "NewBagAfterSearchScannerId")
        self.navigationController?.pushViewController(push, animated: true)
        
        
        
        
        
        
        
        
        
    }
    
    /*@objc func captureBarcode() {
        
        self.viewbg.backgroundColor = UIColor.black
        
        captureSession = AVCaptureSession()

        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else { return }
        let videoInput: AVCaptureDeviceInput

        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            return
        }

        if (captureSession.canAddInput(videoInput)) {
            captureSession.addInput(videoInput)
        } else {
            failed()
            return
        }

        let metadataOutput = AVCaptureMetadataOutput()

        if (captureSession.canAddOutput(metadataOutput)) {
            captureSession.addOutput(metadataOutput)

            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = [.qr, .ean8, .ean13, .pdf417]
            
        } else {
            failed()
            return
        }

        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = self.viewbg.layer.bounds
        previewLayer.videoGravity = .resizeAspectFill
        self.viewbg.layer.addSublayer(previewLayer)

        captureSession.startRunning()
        
    }
    
    func failed() {
        let ac = UIAlertController(title: "Scanning not supported", message: "Your device does not support scanning a code from an item. Please use a device with a camera.", preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        present(ac, animated: true)
        captureSession = nil
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        if (captureSession?.isRunning == true) {
            captureSession.stopRunning()
        }
    }

    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        captureSession.stopRunning()

        /*if let metadataObject = metadataObjects.first {
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
            guard let stringValue = readableObject.stringValue else { return }
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            found(code: stringValue)
            
            let descriptor = readableObject.descriptor as? CIAztecCodeDescriptor
            
            if descriptor == nil {
                print("no bytes")
            } else {
                let rawBytes = descriptor!.errorCorrectedPayload
                print(rawBytes)
            }
            
        }*/

        
        print(metadataObjects)
             
        if let metadataObject = metadataObjects.first {
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
            guard let stringValue = readableObject.stringValue else { return }
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            
            print(stringValue as Any)
            
            let descriptor = readableObject.descriptor as? CIAztecCodeDescriptor
            
            if descriptor == nil {
                print("no bytes")
            } else {
                let rawBytes = descriptor!.errorCorrectedPayload
                print(rawBytes)
            }
            
        }
        
        for metadataObject in metadataObjects {
            print("type: " + metadataObject.type.rawValue)
                    
            let readableObject = metadataObject as! AVMetadataMachineReadableCodeObject
            guard let stringValue = readableObject.stringValue else { return }
            let rawReadableObjectTemp = readableObject.value(forKeyPath: "_internal.basicDescriptor")! as! [String:Any]
            let rawReadableObject = rawReadableObjectTemp["BarcodeRawData"] as? Data
                    
            if let rawBytes = rawReadableObject {
                print(rawBytes)
                print(type(of: rawBytes))
                self.scannedDataIs = rawBytes
                let dataString = rawBytes.base64EncodedString()
                print("dataString: \(dataString)")
                        
                let base64String = "\(dataString)"
                if let data = Data(base64Encoded: base64String) {
                    if let string = String(data: data, encoding: .utf8) {
                        print(string)
                        
                        // print(self.scannedDataIs)
                    }
                }
                
            }
            
            // found(code: stringValue, strRawBytes: self.scannedDataIs, strTypeOrId: metadataObject.type.rawValue)
            self.isThisBagAlreadyExist(code: stringValue, strRawBytes: self.scannedDataIs, strTypeOrId: metadataObject.type.rawValue)
        }
        
        
        
        
        // print("last ?")
        
        /**/
    }*/

    // MARK:- CHECK BAG ID ( ALREADY EXIST ) -
    @objc func isThisBagAlreadyExist(code: String,strRawBytes:String,strTypeOrId:String) {
        
        print(code as Any)
        print(strRawBytes as Any)
        print(strTypeOrId as Any)
        
        self.view.endEditing(true)
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
        
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
         // let str:String = person["role"] as! String
        
            let x : Int = person["userId"] as! Int
            let myString = String(x)
            
            let params = CheckBagExistWB(action: "checkbag",
                                         userId: String(myString),
                                         BAGID: String(code))
        
            print(params as Any)
            
            AF.request(APPLICATION_BASE_URL,
                       method: .post,
                       parameters: params,
                       encoder: JSONParameterEncoder.default).responseJSON { response in
                    // debugPrint(response.result)
                    
                        switch response.result {
                        case let .success(value):
                        
                            let JSON = value as! NSDictionary
                            print(JSON as Any)
                        
                            var strSuccess : String!
                            strSuccess = JSON["status"]as Any as? String
                        
                        // var strSuccess2 : String!
                        // strSuccess2 = JSON["msg"]as Any as? String
                        
                        /*
                         
                         */
                        
                        if strSuccess == String("Success") {
                            print("yes")
                            ERProgressHud.sharedInstance.hide()
                           
                            //
 
                            // print(self.arrTotalBarCode as Any)
                            
                            // print(String(code) as Any)
                            
                            if self.arrTotalBarCode.count == 0 {
                                
                                let myDictionary: [String:Any] = [
                                    "tagSaved"              : String("no"),
                                    "tagName"               : String(code),
                                    "date_harvest"          : String(""),
                                    "destination_address"   : String(""),
                                    "destination_entity"    : String(""),
                                    "destination_number"    : String(""),
                                    "entity_phone"          : String(""),
                                    "facility_location"     : String(""),
                                    "manifest_number"       : String(""),
                                    "name"                  : String(""),
                                    "notes"                 : String(""),
                                    "originating_entity"    : String(""),
                                    "room_number"           : String(""),
                                    "strain"                : String(""),
                                    "tranported_lic"        : String(""),
                                    "transporter_number"    : String(""),
                                    "trasnporter_name"      : String(""),
                                    "raw_bytes"             : String(strRawBytes),
                                    "ec_level"              : String("null"),
                                    "fomatId"               : String(strTypeOrId),
                                    "orientation"           : String(self.deviceOrientation)
                                ]
                                
                                var res = [[String: Any]]()
                                res.append(myDictionary)
                                
                                self.arrTotalBarCode.addObjects(from: res)
                                
                                // print(self.arrTotalBarCode as Any)
                               
                                self.captureSession.stopRunning()
                                
                                let defaults = UserDefaults.standard
                                defaults.set("", forKey: "keySaveSubScanBagCategory")
                                defaults.set(nil, forKey: "keySaveSubScanBagCategory")
                                
                                self.reloadTableviewAfterScanSuccess()
                                
                            } else {
                                
                                for indexx in 0..<self.arrTotalBarCode.count {
                                    
                                    let item = self.arrTotalBarCode[indexx] as? [String:Any]
                                    
                                    // print(item!["tagName"] as! String)
                                    if (item!["tagName"] as! String) == String(code) {
                                        
                                        // print("this bag already exist")
                                                                                
                                        self.checkThisTagIsScannedLocally = "yes"
                                        
                                    } else {
                                       
                                    }
                                    
                                    
                                    
                                    
                                    
                                } // for loop
                                
                                if self.checkThisTagIsScannedLocally == "no" {
                                    
                                    let myDictionary: [String:Any] = [
                                        "tagSaved"              : String("no"),
                                        "tagName"               : String(code),
                                        "date_harvest"          : String(""),
                                        "destination_address"   : String(""),
                                        "destination_entity"    : String(""),
                                        "destination_number"    : String(""),
                                        "entity_phone"          : String(""),
                                        "facility_location"     : String(""),
                                        "manifest_number"       : String(""),
                                        "name"                  : String(""),
                                        "notes"                 : String(""),
                                        "originating_entity"    : String(""),
                                        "room_number"           : String(""),
                                        "strain"                : String(""),
                                        "tranported_lic"        : String(""),
                                        "transporter_number"    : String(""),
                                        "trasnporter_name"      : String(""),
                                        "raw_bytes"             : String(strRawBytes),
                                        "ec_level"              : String("null"),
                                        "fomatId"               : String(strTypeOrId),
                                        "orientation"           : String(self.deviceOrientation)
                                    ]
                                    
                                    var res = [[String: Any]]()
                                    res.append(myDictionary)
                                    
                                    self.arrTotalBarCode.addObjects(from: res)
                                    
                                    // print(self.arrTotalBarCode as Any)
                                   
                                    self.captureSession.stopRunning()
                                    
                                    let defaults = UserDefaults.standard
                                    defaults.set("", forKey: "keySaveSubScanBagCategory")
                                    defaults.set(nil, forKey: "keySaveSubScanBagCategory")
                                    
                                    self.reloadTableviewAfterScanSuccess()
                                    
                                }
                                
                                
                                
                            }
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                        } else {
                            print("no")
                            ERProgressHud.sharedInstance.hide()
                            
                            var strSuccess2 : String!
                            strSuccess2 = JSON["msg"]as Any as? String
                            
                            let alert = UIAlertController(title: "Alert", message: strSuccess2,
                                                          preferredStyle: UIAlertController.Style.alert)

                            alert.addAction(UIAlertAction(title: "Ok",
                                                          style: UIAlertAction.Style.default,
                                                          handler: {(_: UIAlertAction!) in
                                                            
                                                            self.captureSession.startRunning()
                                                            
                                                            
                            }))
                            self.present(alert, animated: true, completion: nil)
                            
                        }
                        
                    case let .failure(error):
                        print(error)
                        ERProgressHud.sharedInstance.hide()
                        
                        Utils.showAlert(alerttitle: SERVER_ISSUE_TITLE, alertmessage: SERVER_ISSUE_MESSAGE, ButtonTitle: "Ok", viewController: self)
                    }
        }
    }
        
    }

    @objc func reloadTableviewAfterScanSuccess() {
        
        self.viewbg.isHidden = true
        self.imgBarCodePlaceholder.isHidden = false
        
        // print(self.arrTotalBarCode as Any)
        
        let array: [NSDictionary] = self.arrTotalBarCode.copy() as! [NSDictionary]
        print(array as Any)
        
        let numbers = array
        let unique = numbers.removingDuplicates()
        // print(unique as Any)
        
        print(type(of: unique))
        
        self.arrListOfBarCodeUpload.add(unique)
        
        self.tbleView.reloadData()
        
    }
    
    func found(code: String,strRawBytes:Data,strTypeOrId:String) {
        print("'Teacher Id Is'==> "+code)
        print(strRawBytes)
        print(strTypeOrId as Any)
        
        print(type(of: code))
        
        /*let myDictionary: [String:Any] = [
            "tagSaved"              : String("no"),
            "tagName"               : String(code),
            "date_harvest"          : String(""),
            "destination_address"   : String(""),
            "destination_entity"    : String(""),
            "destination_number"    : String(""),
            "entity_phone"          : String(""),
            "facility_location"     : String(""),
            "manifest_number"       : String(""),
            "name"                  : String(""),
            "notes"                 : String(""),
            "originating_entity"    : String(""),
            "room_number"           : String(""),
            "strain"                : String(""),
            "tranported_lic"        : String(""),
            "transporter_number"    : String(""),
            "trasnporter_name"      : String(""),
            "raw_bytes"             : "\(self.scannedDataIs)",
            "ec_level"              : String("null"),
            "fomatId"               : String(strTypeOrId),
            "orientation"           : String(self.deviceOrientation)
        ]
        
        var res = [[String: Any]]()
        res.append(myDictionary)
        
        self.arrTotalBarCode.addObjects(from: res)
        print(self.arrTotalBarCode as Any)
       
        captureSession.stopRunning()*/
        
     }
    
}


extension NewBagAfterSearch: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrTotalBarCode.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cell:NewBagAfterSearchTableCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! NewBagAfterSearchTableCell
        
        
          
        let item = arrTotalBarCode[indexPath.row] as? [String:Any]
        cell.lblTag.text = "Tag #"+String(indexPath.row)+" : "+(item!["tagName"] as! String)
        
        if (item!["tagSaved"] as! String) == "yes" {
            cell.backgroundColor = .systemTeal
        } else {
            cell.backgroundColor = .clear
        }
        
        return cell
        
    }

    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView .deselectRow(at: indexPath, animated: true)
        
        // print(self.arrTotalBarCode as Any)
        /*self.arrTotalBarCode.removeObject(at: 2)
        
        let myDictionary: [String:String] = [
            "tagSaved":String("yes"),
            "tagName":String("code"),
        ]
        self.arrTotalBarCode.insert(myDictionary, at: 2)
        
        self.tbleView.reloadData()*/
        
        /*
         var mainBagIdIs:String!
         var selectedBagIdIs:String!
         var selectedIndex:String!
         */
        
        let item = arrTotalBarCode[indexPath.row] as? [String:Any]
        
        let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "FormForBagId") as? FormForBag
        
        push!.mainBagIdIs = String(self.strMainBagId)
        push!.selectedBagIdIs = (item!["tagName"] as! String)
        push!.selectedIndex = indexPath.row
        push!.arrGetFullListOfBags = self.arrTotalBarCode
        self.navigationController?.pushViewController(push!, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
}

extension NewBagAfterSearch: UITableViewDelegate {
    
}


extension Array where Element: Hashable {
    func removingDuplicates() -> [Element] {
        var addedDict = [Element: Bool]()

        return filter {
            addedDict.updateValue(true, forKey: $0) == nil
        }
    }

    mutating func removeDuplicates() {
        self = self.removingDuplicates()
    }
}

extension Data {
    struct HexEncodingOptions: OptionSet {
        let rawValue: Int
        static let upperCase = HexEncodingOptions(rawValue: 1 << 0)
    }

    func hexEncodedString(options: HexEncodingOptions = []) -> String {
        let format = options.contains(.upperCase) ? "%02hhX" : "%02hhx"
        return self.map { String(format: format, $0) }.joined()
    }
}
