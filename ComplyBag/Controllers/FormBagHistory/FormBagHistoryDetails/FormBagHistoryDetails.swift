//
//  FormBagHistoryDetails.swift
//  ComplyBag
//
//  Created by Apple on 13/01/21.
//

import UIKit
import Alamofire

class FormBagHistoryDetails: UIViewController {
    
    // ***************************************************************** // nav
    
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_COLOR
        }
    }
    
    @IBOutlet weak var btnBack:UIButton! {
        didSet {
            btnBack.tintColor = NAVIGATION_BACK_COLOR
        }
    }
    
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "SCAN HISTORY"
            lblNavigationTitle.textColor = NAVIGATION_TITLE_COLOR
            lblNavigationTitle.backgroundColor = .clear
        }
    }
    
    // ***************************************************************** // nav
    
    let cellReuseIdentifier = "formBagHistoryDetailsTableCell"
    
    @IBOutlet weak var imgBarCodePlaceholder:UIImageView! {
        didSet {
            imgBarCodePlaceholder.backgroundColor = .white
        }
    }
    
    @IBOutlet weak var viewbg:UIView! {
        didSet {
            viewbg.backgroundColor = .white
        }
    }
    
    var dict:NSDictionary!
    
    
    // MARK:- ARRAY -
    var arrListOfAllMyOrders:NSMutableArray! = []
    
    // MARK:- TABLE VIEW -
    @IBOutlet weak var tbleView: UITableView! {
        didSet {
            // self.tbleView.delegate = self
            // self.tbleView.dataSource = self
            self.tbleView.backgroundColor = .white
            self.tbleView.tableFooterView = UIView.init(frame: CGRect(origin: .zero, size: .zero))
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.view.backgroundColor = .white
        
        self.viewbg.isHidden = true
        
        self.tbleView.separatorColor = .clear
        // print(dict as Any)
        
        /*
         BAGID = " QR_CODE";
         TAG = "F767-348G56";
         created = "2021-01-08 16:30:08";
         image = "";
         scanId = 22;
         totalNumber = 13;
         */
        
        self.btnBack.addTarget(self, action: #selector(backCLickMethod), for: .touchUpInside)
        
        self.imgBarCodePlaceholder.isUserInteractionEnabled = false
        
        self.bagDetails()
    }
    
    @objc func backCLickMethod() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func bagDetails() {
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
        self.arrListOfAllMyOrders.removeAllObjects()
        
        self.view.endEditing(true)
        
        // let x : Int = (productDetailsId!)
        //let myString = String(x)
        
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            // let str:String = person["role"] as! String
            
            let x : Int = person["userId"] as! Int
            let myString = String(x)
            
            // let x2 : Int = person["userId"] as! Int
            // let myString2 = String(x2)
            
            let params = FormBagHistoryDetailsParam(action: "scandetails",
                                                    userId: String(myString),
                                                    bagId: (dict["BAGID"] as! String))
            
            print(params as Any)
            
            AF.request(APPLICATION_BASE_URL,
                       method: .post,
                       parameters: params,
                       encoder: JSONParameterEncoder.default).responseJSON { response in
                // debugPrint(response.result)
                
                switch response.result {
                case let .success(value):
                    
                    let JSON = value as! NSDictionary
                    print(JSON as Any)
                    
                    var strSuccess : String!
                    strSuccess = JSON["status"]as Any as? String
                    
                    // var strSuccess2 : String!
                    // strSuccess2 = JSON["msg"]as Any as? String
                    
                    if strSuccess == String("success") {
                        print("yes")
                        ERProgressHud.sharedInstance.hide()
                        
                        var ar : NSArray!
                        ar = (JSON["data"] as! Array<Any>) as NSArray
                        self.arrListOfAllMyOrders.addObjects(from: ar as! [Any])
                        
                        // self.totalItemsInCart()
                        
                        self.tbleView.delegate = self
                        self.tbleView.dataSource = self
                        self.tbleView.reloadData()
                        
                    } else {
                        print("no")
                        ERProgressHud.sharedInstance.hide()
                        
                        var strSuccess2 : String!
                        strSuccess2 = JSON["msg"]as Any as? String
                        
                        Utils.showAlert(alerttitle: String(strSuccess), alertmessage: String(strSuccess2), ButtonTitle: "Ok", viewController: self)
                        
                    }
                    
                case let .failure(error):
                    print(error)
                    ERProgressHud.sharedInstance.hide()
                    
                    Utils.showAlert(alerttitle: SERVER_ISSUE_TITLE, alertmessage: SERVER_ISSUE_MESSAGE, ButtonTitle: "Ok", viewController: self)
                }
            }
            
        }
    }
}

extension FormBagHistoryDetails: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrListOfAllMyOrders.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cell:FormBagHistoryDetailsTableCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! FormBagHistoryDetailsTableCell
        
        
          
        let item = self.arrListOfAllMyOrders[indexPath.row] as? [String:Any]
        print(item as Any)
        // var ar : NSArray!
        // ar = (item!["scanResponse"] as! Array<Any>) as NSArray
        // self.arrListOfAllMyOrders.addObjects(from: ar as! [Any])
        
        cell.lblTag.text = "Tag #"+String(indexPath.row+1)+" : "+(item!["TAG"] as! String)
        cell.backgroundColor = .clear
        
        /*if (item!["tagSaved"] as! String) == "yes" {
            cell.backgroundColor = .systemTeal
        } else {
            cell.backgroundColor = .clear
        }*/
        
        return cell
        
    }

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView .deselectRow(at: indexPath, animated: true)
        
        let item = self.arrListOfAllMyOrders[indexPath.row] as? [String:Any]
        
        // var ar : NSArray!
        // ar = (item!["scanResponse"] as! Array<Any>) as NSArray
        // self.arrListOfAllMyOrders.addObjects(from: ar as! [Any])
        
        let alert = UIAlertController(title: "Tag #"+String(indexPath.row+1)+" : "+(item!["TAG"] as! String), message: nil, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "View details", style: .default, handler: { action in
            
            if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
                
                let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "FormBagSubDetailsId") as? FormBagSubDetails
            
                push!.dictGetAllSubBagDetails = item as NSDictionary?
                push!.disableTextFieldsOrNot = "yes"
                push!.mainBagIdIs = "\(item!["BAGID"] as! String)"
                push!.getScanId = "\(item!["scanId"] as! Int)"
                push!.getUserIdId = "\(person["userId"] as! Int)"
            
                self.navigationController?.pushViewController(push!, animated: true)
                
            }
            
        }))
        
        alert.addAction(UIAlertAction(title: "Edit details", style: .default, handler: { action in
            
            if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
                
                let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "FormBagSubDetailsId") as? FormBagSubDetails
            
                push!.dictGetAllSubBagDetails = item as NSDictionary?
                push!.disableTextFieldsOrNot = "noCall"
                push!.mainBagIdIs = "\(item!["BAGID"] as! String)"
                push!.getScanId = "\(item!["scanId"] as! Int)"
                push!.getUserIdId = "\(person["userId"] as! Int)"
            
                self.navigationController?.pushViewController(push!, animated: true)
                
            }
            
        }))
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .destructive, handler: { action in
        }))
        self.present(alert, animated: true, completion: nil)
        
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100-20
    }
    
}

extension FormBagHistoryDetails: UITableViewDelegate {
    
}


 
