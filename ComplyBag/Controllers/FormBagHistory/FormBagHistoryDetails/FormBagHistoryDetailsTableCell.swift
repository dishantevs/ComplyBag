//
//  FormBagHistoryDetailsTableCell.swift
//  ComplyBag
//
//  Created by Apple on 13/01/21.
//

import UIKit

class FormBagHistoryDetailsTableCell: UITableViewCell {

    @IBOutlet weak var viewBg:UIView! {
        didSet {
            viewBg.layer.cornerRadius = 6
            viewBg.clipsToBounds = true
            viewBg.backgroundColor = UIColor(red: 223.0/255.0, green: 223.0/255.0, blue: 223.0/255.0, alpha: 1)
        }
    }
    
    @IBOutlet weak var imgTag:UIImageView!
    @IBOutlet weak var lblTag:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
