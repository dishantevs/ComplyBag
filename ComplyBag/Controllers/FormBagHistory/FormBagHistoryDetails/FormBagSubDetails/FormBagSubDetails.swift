//
//  FormBagSubDetails.swift
//  ComplyBag
//
//  Created by Apple on 13/01/21.
//

import UIKit
import Alamofire
import SwiftyJSON

class FormBagSubDetails: UIViewController {
    
    // ***************************************************************** // nav
                    
        @IBOutlet weak var navigationBar:UIView! {
            didSet {
                navigationBar.backgroundColor = NAVIGATION_COLOR
            }
        }
            
        @IBOutlet weak var btnBack:UIButton! {
            didSet {
                btnBack.tintColor = NAVIGATION_BACK_COLOR
            }
        }
            
        @IBOutlet weak var lblNavigationTitle:UILabel! {
            didSet {
                lblNavigationTitle.text = NEW_BAG_PAGE_NAVIGATION_TITLE
                lblNavigationTitle.textColor = NAVIGATION_TITLE_COLOR
                lblNavigationTitle.backgroundColor = .clear
            }
        }
                    
    // ***************************************************************** // nav
    
    let cellReuseIdentifier = "formBagSubDetailsTableCell"
    
    var mainBagIdIs:String!
    var selectedBagIdIs:String!
    var selectedIndex:Int!
    var arrGetFullListOfBags:NSMutableArray! = []
    
    var strNavigationTitle:String!
    var getScanId:String!
    var getUserIdId:String!
    
    var disableTextFieldsOrNot:String!
    
    var arrSavedValueAndSendToServer:NSMutableArray! = []
    
    var dictGetAllSubBagDetails:NSDictionary!
    
    /*
     "raw_bytes"             : (item!["raw_bytes"] as! String),
     "ec_level"              : (item!["ec_level"] as! String),
     "fomatId"               : (item!["fomatId"] as! String),
     "orientation"           : (item!["orientation"] as! String)
     */
    var strRawBytes:String!
    var strEcLevel:String!
    var strFormatId:String!
    var strOrientation:String!
    
    
    // MARK:- TABLE VIEW -
    @IBOutlet weak var tbleView: UITableView! {
        didSet {
            self.tbleView.delegate = self
            self.tbleView.dataSource = self
            self.tbleView.backgroundColor = .white
            self.tbleView.tableFooterView = UIView.init(frame: CGRect(origin: .zero, size: .zero))
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.btnBack.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
        
        // print(mainBagIdIs as Any)
        // print(selectedBagIdIs as Any)
        // print(selectedIndex as Any)
        // print(arrGetFullListOfBags as Any)
        
        if self.disableTextFieldsOrNot == "noCall" {
            
            self.isUserIntractionEnable(setBool: true)
            self.fetchAllDataFromServer()
            
        } else if self.disableTextFieldsOrNot == "yes" {
            
            
            self.isUserIntractionEnable(setBool: false)
            
        } else {
            
            
            self.isUserIntractionEnable(setBool: true)
            
        }
        
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
        self.getScannedBagDetailsWB()
        
        
    }

    @objc func backClickMethod() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func fetchAllDataFromServer() {
        self.lblNavigationTitle.text = "TAG: #"+(self.dictGetAllSubBagDetails["TAG"] as! String)
        
        // print(dictGetAllSubBagDetails as Any)
        
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! FormBagSubDetailsTableCell
        
        var ar : NSArray!
        ar = (dictGetAllSubBagDetails!["scanResponse"] as! Array<Any>) as NSArray
        
        let item = ar[0] as? [String:Any]
        
        self.strRawBytes = (item!["raw_bytes"] as! String)
        self.strEcLevel = (item!["ec_level"] as! String)
        self.strFormatId = (item!["fomatId"] as! String)
        self.strOrientation = (item!["orientation"] as! String)
        
        cell.txtDateOfHarvest.text!                 = (item!["date_harvest"] as! String)
        cell.txtDestinationAddress.text!            = (item!["destination_address"] as! String)
        cell.txtDestinationEntity.text!             = (item!["destination_entity"] as! String)
        cell.txtDestinationPhoneNumber.text!        = (item!["destination_number"] as! String)
        cell.txtOriginatingEntityPhoneNumber.text!  = (item!["entity_phone"] as! String)
        cell.txtFacilityLocation.text!              = (item!["facility_location"] as! String)
        cell.txtMenifestEntiry.text!                = (item!["manifest_number"] as! String)
        cell.txtName.text!                          = (item!["name"] as! String)
        cell.txtNotes.text!                         = (item!["notes"] as! String)
        cell.txtOriginatingEntity.text!             = (item!["originating_entity"] as! String)
        cell.txtRoomNumber.text!                    = (item!["room_number"] as! String)
        cell.txtStrain.text!                        = (item!["strain"] as! String)
        cell.txtTransporterLICnumber.text!          = (item!["tranported_lic"] as! String)
        cell.txtTransporterPhoneNumber.text!        = (item!["transporter_number"] as! String)
        cell.txtTransporterName.text!               = (item!["trasnporter_name"] as! String)
        
        
        /*let myDictionary: [String:String] = [
            "date_harvest"          :String(cell.txtDateOfHarvest.text!),
            "destination_address"   :String(cell.txtDestinationAddress.text!),
            "destination_entity"    :String(cell.txtDestinationEntity.text!),
            "destination_number"    :String(cell.txtDestinationPhoneNumber.text!),
            "entity_phone"          :String(cell.txtOriginatingEntityPhoneNumber.text!),
            "facility_location"     :String(cell.txtFacilityLocation.text!),
            "manifest_number"       :String(cell.txtMenifestEntiry.text!),
            "name"                  :String(cell.txtName.text!),
            "notes"                 :String(cell.txtNotes.text!),
            "originating_entity"    :String(cell.txtOriginatingEntity.text!),
            "room_number"           :String(cell.txtRoomNumber.text!),
            "strain"                :String(cell.txtStrain.text!),
            "tranported_lic"        :String(cell.txtTransporterLICnumber.text!),
            "transporter_number"    :String(cell.txtTransporterPhoneNumber.text!),
            "trasnporter_name"      :String(cell.txtTransporterName.text!),
            "raw_bytes"             : (item!["raw_bytes"] as! String),
            "ec_level"              : (item!["ec_level"] as! String),
            "fomatId"               : (item!["fomatId"] as! String),
            "orientation"           : (item!["orientation"] as! String)
        ]
        
        var res = [[String: String]]()
        res.append(myDictionary)
        
        self.arrSavedValueAndSendToServer.addObjects(from: res)*/
        
    }
    
    
    @objc func isUserIntractionEnable(setBool:Bool) {
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! FormBagSubDetailsTableCell
        
        cell.txtDateOfHarvest.isUserInteractionEnabled = setBool
        cell.txtDestinationAddress.isUserInteractionEnabled = setBool
        cell.txtDestinationEntity.isUserInteractionEnabled = setBool
        cell.txtDestinationPhoneNumber.isUserInteractionEnabled = setBool
        cell.txtOriginatingEntityPhoneNumber.isUserInteractionEnabled = setBool
        cell.txtFacilityLocation.isUserInteractionEnabled = setBool
        cell.txtMenifestEntiry.isUserInteractionEnabled = setBool
        cell.txtName.isUserInteractionEnabled = setBool
        cell.txtNotes.isUserInteractionEnabled = setBool
        cell.txtOriginatingEntity.isUserInteractionEnabled = setBool
        cell.txtRoomNumber.isUserInteractionEnabled = setBool
        cell.txtStrain.isUserInteractionEnabled = setBool
        cell.txtTransporterLICnumber.isUserInteractionEnabled = setBool
        cell.txtTransporterPhoneNumber.isUserInteractionEnabled = setBool
        cell.txtTransporterName.isUserInteractionEnabled = setBool
        
        if self.disableTextFieldsOrNot == "noCall" {
            
            cell.btnSubmit.isHidden = false
            cell.btnDateOfHarvest.isHidden = false
            
        } else if self.disableTextFieldsOrNot == "yes" {
            
            cell.btnSubmit.isHidden = true
            cell.btnDateOfHarvest.isHidden = true
            
        } else {
            
            cell.btnSubmit.isHidden = false
            cell.btnDateOfHarvest.isHidden = false
            
        }
        
        
    }
    
    @objc func getScannedBagDetailsWB() {
        
        self.view.endEditing(true)
        
        let params = ScannedBagWB(action: "scandetails",
                                  bagId: String(self.mainBagIdIs),
                                  userId: String(self.getUserIdId))
        
        print(params as Any)
        
        AF.request(APPLICATION_BASE_URL,
                   method: .post,
                   parameters: params,
                   encoder: JSONParameterEncoder.default).responseJSON { response in
            // debugPrint(response.result)
            
            switch response.result {
            case let .success(value):
                
                let JSON = value as! NSDictionary
                print(JSON as Any)
                
                var strSuccess : String!
                strSuccess = JSON["status"]as Any as? String
                
                if strSuccess == String("success") {
                    print("yes")
                    ERProgressHud.sharedInstance.hide()
                    
                    var ar : NSArray!
                    ar = (JSON["data"] as! Array<Any>) as NSArray
                    
                    let item = ar[0] as? [String:Any]
                    
                    /*var ar2 : NSArray!
                     ar2 = (item!["scanResponse"] as! Array<Any>) as NSArray*/
                    
                    // let item2 = ar2[0] as? [String:Any]
                    self.dictGetAllSubBagDetails = item as NSDictionary?
                    
                    self.fetchAllDataFromServer()
                    
                } else {
                    
                    print("no")
                    ERProgressHud.sharedInstance.hide()
                    
                    var strSuccess2 : String!
                    strSuccess2 = JSON["msg"]as Any as? String
                    
                    let alert = UIAlertController(title: "Error", message: strSuccess2,
                                                  preferredStyle: UIAlertController.Style.alert)
                    
                    alert.addAction(UIAlertAction(title: "Dismiss",
                                                  style: .destructive,
                                                  handler: {(_: UIAlertAction!) in
                        
                        
                    }))
                    
                    self.present(alert, animated: true, completion: nil)
                    
                    
                }
                
            case let .failure(error):
                print(error)
                ERProgressHud.sharedInstance.hide()
                
                Utils.showAlert(alerttitle: SERVER_ISSUE_TITLE, alertmessage: SERVER_ISSUE_MESSAGE, ButtonTitle: "Ok", viewController: self)
            }
        }
        
    }
    
    @objc func fetchEditScannedBag() {
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! FormBagSubDetailsTableCell
        
        self.self.arrSavedValueAndSendToServer.removeAllObjects()
        
        let myDictionary: [String:String] = [
            "date_harvest"          : String(cell.txtDateOfHarvest.text!),
            "destination_address"   : String(cell.txtDestinationAddress.text!),
            "destination_entity"    : String(cell.txtDestinationEntity.text!),
            "destination_number"    : String(cell.txtDestinationPhoneNumber.text!),
            "entity_phone"          : String(cell.txtOriginatingEntityPhoneNumber.text!),
            "facility_location"     : String(cell.txtFacilityLocation.text!),
            "manifest_number"       : String(cell.txtMenifestEntiry.text!),
            "name"                  : String(cell.txtName.text!),
            "notes"                 : String(cell.txtNotes.text!),
            "originating_entity"    : String(cell.txtOriginatingEntity.text!),
            "room_number"           : String(cell.txtRoomNumber.text!),
            "strain"                : String(cell.txtStrain.text!),
            "tranported_lic"        : String(cell.txtTransporterLICnumber.text!),
            "transporter_number"    : String(cell.txtTransporterPhoneNumber.text!),
            "trasnporter_name"      : String(cell.txtTransporterName.text!),
            "raw_bytes"             : String(self.strRawBytes),
            "ec_level"              : String(self.strEcLevel),
            "fomatId"               : String(self.strFormatId),
            "orientation"           : String(self.strOrientation)
        ]
        
        var res = [[String: String]]()
        res.append(myDictionary)
        
        self.arrSavedValueAndSendToServer.addObjects(from: res)
        
        self.editScannedBagDetailsWB()
    }
    
    @objc func editScannedBagDetailsWB() {
        
        self.view.endEditing(true)
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
        
        let paramsArray = self.arrSavedValueAndSendToServer
        let paramsJSON = JSON(paramsArray as Any)
        let paramsString = paramsJSON.rawString(String.Encoding.utf8, options: JSONSerialization.WritingOptions.prettyPrinted)!
        
            
        let params = EditScannedBagWB(action: "editscan",
                                      scanId: String(self.getScanId),
                                      userId: String(self.getUserIdId),
                                      scanResponse:String(paramsString))
        
        print(params as Any)
            
        AF.request(APPLICATION_BASE_URL,
                   method: .post,
                   parameters: params,
                   encoder: JSONParameterEncoder.default).responseJSON { response in
                    // debugPrint(response.result)
                    
                    switch response.result {
                    case let .success(value):
                        
                        let JSON = value as! NSDictionary
                        print(JSON as Any)
                        
                        var strSuccess : String!
                        strSuccess = JSON["status"]as Any as? String
                       
                        var strSuccessMessage : String!
                        strSuccessMessage = JSON["msg"]as Any as? String
                        
                        if strSuccess == String("Success") {
                            print("yes")
                            ERProgressHud.sharedInstance.hide()
                            
                            let alert = UIAlertController(title: strSuccess, message: String(strSuccessMessage),         preferredStyle: .alert)

                            alert.addAction(UIAlertAction(title: "Ok",
                                                          style: UIAlertAction.Style.default,
                                                          handler: {(_: UIAlertAction!) in
                                                            
                                                            self.getScannedBagDetailsWB()
                                                            
                            }))
                            self.present(alert, animated: true, completion: nil)
                            
                            
                              
                        } else {
                            
                            print("no")
                            ERProgressHud.sharedInstance.hide()
                            
                            var strSuccess2 : String!
                            strSuccess2 = JSON["msg"]as Any as? String
                            
                            let alert = UIAlertController(title: "Error", message: strSuccess2,
                                                          preferredStyle: UIAlertController.Style.alert)
 
                            alert.addAction(UIAlertAction(title: "Dismiss",
                                                          style: .destructive,
                                                          handler: {(_: UIAlertAction!) in
                                                            
                                                          }))
                                
                            self.present(alert, animated: true, completion: nil)
                             
                        }
                        
                    case let .failure(error):
                        print(error)
                        ERProgressHud.sharedInstance.hide()
                        
                        Utils.showAlert(alerttitle: SERVER_ISSUE_TITLE, alertmessage: SERVER_ISSUE_MESSAGE, ButtonTitle: "Ok", viewController: self)
                    }
                   }
        
    }
    
}

extension FormBagSubDetails: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cell:FormBagSubDetailsTableCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! FormBagSubDetailsTableCell
        
        cell.backgroundColor = .clear
        
        let backgroundView = UIView()
        backgroundView.backgroundColor = .clear
        cell.selectedBackgroundView = backgroundView
        
        if self.disableTextFieldsOrNot == "yes" {
            cell.btnSubmit.addTarget(self, action: #selector(getScannedBagDetailsWB), for: .touchUpInside)
        } else {
            cell.btnSubmit.addTarget(self, action: #selector(fetchEditScannedBag), for: .touchUpInside)
        }
        
        // cell.btnDateOfHarvest.addTarget(self, action: #selector(dateOfHarvestClickMethod), for: .touchUpInside)
        
        return cell
        
    }
    
    /*@objc func dateOfHarvestClickMethod() {
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! FormForBagTableCell
        
        RPicker.selectDate(title: "Select Date", didSelectDate: {[weak self] (selectedDate) in
            // TODO: Your implementation for date
            // self?.outputLabel.text = selectedDate.dateString("MMM-dd-YYYY")
            cell.txtDateOfHarvest.text = selectedDate.dateString("dd/MMM/YYYY")
            self?.tbleView.reloadData()
        })
    }*/

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView .deselectRow(at: indexPath, animated: true)
        
         
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    
        if self.disableTextFieldsOrNot == "yes" {
            return 1100
        } else {
            return 1470
        }
        
    }
    
}

extension FormBagSubDetails: UITableViewDelegate {
    
}

 
