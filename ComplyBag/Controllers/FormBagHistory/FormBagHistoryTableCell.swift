//
//  FormBagHistoryTableCell.swift
//  ComplyBag
//
//  Created by Apple on 13/01/21.
//

import UIKit

class FormBagHistoryTableCell: UITableViewCell {

    @IBOutlet weak var viewDateBG:UIView! {
        didSet {
            viewDateBG.layer.cornerRadius = 8
            viewDateBG.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var lblDateUp:UILabel!
    @IBOutlet weak var lblDateDown:UILabel!
    
    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var lblPhoneNumber:UILabel!
    @IBOutlet weak var lblAddress:UILabel! {
        didSet {
            lblAddress.text = "i am aaddresrxtcfvygbh erxctrvybuh njwerxctvyg bhnj wzexrctrvybun exrctvyb"
        }
    }
    
    @IBOutlet weak var lblRequested:UILabel! {
        didSet {
            lblRequested.layer.cornerRadius = 4
            lblRequested.clipsToBounds = true
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
