//
//  FormBagHistory.swift
//  ComplyBag
//
//  Created by Apple on 13/01/21.
//

import UIKit
import Alamofire

class FormBagHistory: UIViewController {

    // ***************************************************************** // nav
                    
        @IBOutlet weak var navigationBar:UIView! {
            didSet {
                navigationBar.backgroundColor = NAVIGATION_COLOR
            }
        }
            
        @IBOutlet weak var btnBack:UIButton! {
            didSet {
                btnBack.tintColor = NAVIGATION_BACK_COLOR
            }
        }
            
        @IBOutlet weak var lblNavigationTitle:UILabel! {
            didSet {
                lblNavigationTitle.text = SCAN_HISTORY_PAGE_NAVIGATION_TITLE
                lblNavigationTitle.textColor = NAVIGATION_TITLE_COLOR
                lblNavigationTitle.backgroundColor = .clear
            }
        }
                    
    // ***************************************************************** // nav
    let cellReuseIdentifier = "formBagHistoryTableCell"
    
    // MARK:- ARRAY -
    var arrListOfAllMyOrders:NSMutableArray! = []
    
    // MARK:- TABLE VIEW -
    @IBOutlet weak var tbleView: UITableView! {
        didSet {
            // self.tbleView.delegate = self
            // self.tbleView.dataSource = self
            self.tbleView.backgroundColor = .white
            self.tbleView.tableFooterView = UIView.init(frame: CGRect(origin: .zero, size: .zero))
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.btnBack.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
        self.view.backgroundColor = .white
        
        if let myLoadedString = UserDefaults.standard.string(forKey: "keyBackOrSlide") {
            if myLoadedString == "backOrMenu" {
                
                // menu
                btnBack.setImage(UIImage(named: "menu"), for: .normal)
                self.sideBarMenuClick()
                
            } else {
                
                // back
                btnBack.setImage(UIImage(systemName: "arrow.left"), for: .normal)
                btnBack.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
            }
            
         } else {
            
            // back
            btnBack.setImage(UIImage(systemName: "arrow.left"), for: .normal)
            btnBack.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
        }
        
        self.formBagHistory()
    }

    @objc func sideBarMenuClick() {
        
        let defaults = UserDefaults.standard
        defaults.setValue("", forKey: "keyBackOrSlide")
        defaults.setValue(nil, forKey: "keyBackOrSlide")
        
        if revealViewController() != nil {
            btnBack.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
                  
            revealViewController().rearViewRevealWidth = 300
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
    }
    
    @objc func backClickMethod() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func formBagHistory() {
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
        self.arrListOfAllMyOrders.removeAllObjects()
        
        self.view.endEditing(true)
        
        // let x : Int = (productDetailsId!)
         //let myString = String(x)
        
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
        // let str:String = person["role"] as! String
       
           let x : Int = person["userId"] as! Int
           let myString = String(x)
            
            let params = FormBagHistoryParam(action: "scanlist",
                                             userId: String(myString))
            
        print(params as Any)
        
        AF.request(APPLICATION_BASE_URL,
                   method: .post,
                   parameters: params,
                   encoder: JSONParameterEncoder.default).responseJSON { response in
                    // debugPrint(response.result)
                    
                    switch response.result {
                    case let .success(value):
                        
                        let JSON = value as! NSDictionary
                        print(JSON as Any)
                        
                        var strSuccess : String!
                        strSuccess = JSON["status"]as Any as? String
                        
                        // var strSuccess2 : String!
                        // strSuccess2 = JSON["msg"]as Any as? String
                        
                        if strSuccess == String("success") {
                            print("yes")
                            ERProgressHud.sharedInstance.hide()
                           
                            var ar : NSArray!
                            ar = (JSON["data"] as! Array<Any>) as NSArray
                            self.arrListOfAllMyOrders.addObjects(from: ar as! [Any])
                            
                            // self.totalItemsInCart()
                            
                            self.tbleView.delegate = self
                            self.tbleView.dataSource = self
                            self.tbleView.reloadData()
                            
                        } else {
                            print("no")
                            ERProgressHud.sharedInstance.hide()
                            
                            var strSuccess2 : String!
                            strSuccess2 = JSON["msg"]as Any as? String
                            
                            Utils.showAlert(alerttitle: String(strSuccess), alertmessage: String(strSuccess2), ButtonTitle: "Ok", viewController: self)
                            
                        }
                        
                    case let .failure(error):
                        print(error)
                        ERProgressHud.sharedInstance.hide()
                        
                        Utils.showAlert(alerttitle: SERVER_ISSUE_TITLE, alertmessage: SERVER_ISSUE_MESSAGE, ButtonTitle: "Ok", viewController: self)
                    }
        }
        
    }
    }
}

extension FormBagHistory: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrListOfAllMyOrders.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cell:FormBagHistoryTableCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! FormBagHistoryTableCell
        
        cell.backgroundColor = .clear
        
        
         let item = self.arrListOfAllMyOrders[indexPath.row] as? [String:Any]
         // print(item as Any)
        
        // let x222 : Int = (item!["purcheseId"] as! Int)
        // let myString222 = String(x222)
        
         cell.lblName.text    = (item!["BAGID"] as! String)
         
         cell.lblPhoneNumber.text = "Tag : "+(item!["TAG"] as! String)
         
         
         /*
          action: bookinglist
          userId:
          status:  //0=request 1=accepted 2 completed 3=decline
          userType= Driver / Member/
          */
         
        let status : Int = (item!["totalNumber"] as! Int)
        let myStatus = String(status);
        cell.lblAddress.text = "Total items : "+String(myStatus)
        
        cell.lblRequested.isHidden = true
        
         /*if myStatus == "0" {
             
             cell.lblRequested.text = " Requested "
             cell.lblRequested.backgroundColor = .systemYellow
             
         } else if myStatus == "1" {
             
             cell.lblRequested.text = " Accepted "
             cell.lblRequested.backgroundColor = .systemOrange
             
         } else if myStatus == "2" {
             
             cell.lblRequested.text = " Completed "
             cell.lblRequested.backgroundColor = .systemGreen
             
         } else if myStatus == "3" {
             
             cell.lblRequested.text = " Decline "
             cell.lblRequested.backgroundColor = .systemRed
             
         }*/
         
         let fullNameArr = (item!["created"] as! String).components(separatedBy: "-")

         // let expMonth    = fullNameArr[0]
         let dateDown = fullNameArr[1]
         let dateUp = fullNameArr[2]
         
         // print(expMonth as Any)
         // print(dateDown as Any)
         // print(dateUp as Any)
         
         let removeSpaceFromDate = dateUp.components(separatedBy: " ")
         let finalDateUpIs    = removeSpaceFromDate[0]
         cell.lblDateUp.text = String(finalDateUpIs)
         
         
         if String(dateDown) == "1" || String(dateDown) == "01"  {
             
             cell.lblDateDown.text = "JAN"
             
         } else if String(dateDown) == "2"  || String(dateDown) == "02" {
             
             cell.lblDateDown.text = "FEB"
             
         } else if String(dateDown) == "3" || String(dateDown) == "03" {
             
             cell.lblDateDown.text = "MAR"
             
         } else if String(dateDown) == "4" || String(dateDown) == "04" {
             
             cell.lblDateDown.text = "APR"
             
         } else if String(dateDown) == "5" || String(dateDown) == "05" {
             
             cell.lblDateDown.text = "MAY"
             
         } else if String(dateDown) == "6" || String(dateDown) == "06" {
             
             cell.lblDateDown.text = "JUN"
             
         } else if String(dateDown) == "7" || String(dateDown) == "07" {
             
             cell.lblDateDown.text = "JUL"
             
         } else if String(dateDown) == "8" || String(dateDown) == "08" {
             
             cell.lblDateDown.text = "AUG"
             
         } else if String(dateDown) == "9" || String(dateDown) == "09" {
             
             cell.lblDateDown.text = "SEP"
             
         } else if String(dateDown) == "10" {
             
             cell.lblDateDown.text = "OCT"
             
         } else if String(dateDown) == "11" {
             
             cell.lblDateDown.text = "NOV"
             
         } else if String(dateDown) == "12" {
             
             cell.lblDateDown.text = "DEC"
             
         }
        
        
         cell.accessoryType = .disclosureIndicator
         
        
        return cell
        
    }
    
    

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView .deselectRow(at: indexPath, animated: true)
        
        let item = self.arrListOfAllMyOrders[indexPath.row] as? [String:Any]
        let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "FormBagHistoryDetailsId") as? FormBagHistoryDetails
        push!.dict = item as NSDictionary?
        self.navigationController?.pushViewController(push!, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}

extension FormBagHistory: UITableViewDelegate {
    
}

 
