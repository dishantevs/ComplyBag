//
//  CEditTotalTableCell.swift
//  Alien Broccoli
//
//  Created by Apple on 07/10/20.
//

import UIKit

class CEditTotalTableCell: UITableViewCell {

    @IBOutlet weak var imgProfile:UIImageView! {
        didSet {
            imgProfile.layer.cornerRadius = 8
            imgProfile.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var txtEmail:UITextField! {
        didSet {
            txtEmail.isUserInteractionEnabled = false
            txtEmail.textColor = .black
            txtEmail.backgroundColor = .white
            txtEmail.attributedPlaceholder = NSAttributedString(string: "name...",
                                         attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
    }
    @IBOutlet weak var txtUsername:UITextField! {
        didSet {
            txtUsername.textColor = .black
            txtUsername.backgroundColor = .white
            txtUsername.attributedPlaceholder = NSAttributedString(string: "name...",
                                         attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
    }
    @IBOutlet weak var txtPhoneNumber:UITextField! {
        didSet {
            txtPhoneNumber.textColor = .black
            txtPhoneNumber.backgroundColor = .white
            txtPhoneNumber.attributedPlaceholder = NSAttributedString(string: "phone number...",
                                         attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
    }
    @IBOutlet weak var txtAddress:UITextField! {
        didSet {
            txtAddress.textColor = .black
            txtAddress.backgroundColor = .white
            txtAddress.attributedPlaceholder = NSAttributedString(string: "address...",
                                         attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
    }
    
    @IBOutlet weak var btnUpdate:UIButton! {
        didSet {
            btnUpdate.layer.cornerRadius = 4
            btnUpdate.clipsToBounds = true
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
