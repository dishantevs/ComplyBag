//
//  EULA.swift
//  CuckooBirds
//
//  Created by Apple on 22/03/21.
//

import UIKit
import Alamofire

class EULA: UIViewController {

    @IBOutlet weak var btnAgree:UIButton! {
        didSet {
            btnAgree.layer.cornerRadius = 0
            btnAgree.clipsToBounds = true
            btnAgree.backgroundColor = NAVIGATION_COLOR
            btnAgree.setTitle("Yes, I Agree all these terms", for: .normal)
            btnAgree.setTitleColor(.white, for: .normal)
        }
    }
    
    @IBOutlet weak var txtView:UITextView! {
        didSet {
            txtView.isEditable = false
            txtView.backgroundColor = .white
            txtView.textColor = .black
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        let defaults = UserDefaults.standard

        //  defaults.set("", forKey: "keyEULA")
        
        if let myString = defaults.string(forKey: "keyEULA") {
             
            if myString == "eula" {
                
                print("yes, eula")
                let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ContinueAsAId")
                self.navigationController?.pushViewController(push, animated: true)
                
            } else {

                print("no, eula")
                self.terms()
                
            }
            
        } else {
            self.terms()
        }
        
        self.btnAgree.addTarget(self, action: #selector(agreeClickMethod), for: .touchUpInside)
        
    }
    
    @objc func agreeClickMethod() {
        
        let defaults = UserDefaults.standard
        defaults.set("eula", forKey: "keyEULA")
        
        let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ContinueAsAId")
        self.navigationController?.pushViewController(push, animated: true)
        
    }
    
    @objc func terms() {
        
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "please wait...")
            
        self.view.endEditing(true)
            
        // if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            // let x : Int = (person["userId"] as! Int)
            // let myString = String(x)
              
        
            let params = PrivacyWB(action: "privacypolicy")
            
            AF.request(APPLICATION_BASE_URL,
                       method: .post,
                       parameters: params,
                       encoder: JSONParameterEncoder.default).responseJSON { response in
                        // debugPrint(response.result)
                        
                        switch response.result {
                        case let .success(value):
                            
                            let JSON = value as! NSDictionary
                              // print(JSON as Any)
                            
                            var strSuccess : String!
                            strSuccess = JSON["status"]as Any as? String
                            
                             var strSuccess2 : String!
                             strSuccess2 = JSON["msg"]as Any as? String
                            
                            if strSuccess == String("success") {
                                print("yes")
                                 ERProgressHud.sharedInstance.hide()
                               
                                let htmlString = "<span style=\"font-family: Avenir Next;color:black; font-size: 16.0\">\(strSuccess2 ?? "" )</span>"
                                let data = htmlString.data(using: String.Encoding.unicode)! // mind "!"
                                let attrStr = try? NSAttributedString( // do catch
                                    data: data,
                                    options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
                                    documentAttributes: nil)
                                // suppose we have an UILabel, but any element with NSAttributedString will do
                                self.txtView.attributedText = attrStr
                                
                                
                            } else {
                                print("no")
                                ERProgressHud.sharedInstance.hide()
                                
                                var strSuccess2 : String!
                                strSuccess2 = JSON["msg"]as Any as? String
                                
                                Utils.showAlert(alerttitle: String(strSuccess), alertmessage: String(strSuccess2), ButtonTitle: "Ok", viewController: self)
                                
                            }
                            
                        case let .failure(error):
                            print(error)
                            ERProgressHud.sharedInstance.hide()
                            
                            Utils.showAlert(alerttitle: SERVER_ISSUE_TITLE, alertmessage: SERVER_ISSUE_MESSAGE, ButtonTitle: "Ok", viewController: self)
                        }
                }
            
            
    }
    
}
