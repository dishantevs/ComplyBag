//
//  Register.swift
//  ShootWorthy
//
//  Created by Apple on 21/12/20.
//

import UIKit
import Alamofire

class Register: UIViewController, UITextFieldDelegate {

    // ***************************************************************** // nav
                    
        @IBOutlet weak var navigationBar:UIView! {
            didSet {
                navigationBar.backgroundColor = NAVIGATION_COLOR
            }
        }
            
        @IBOutlet weak var btnBack:UIButton! {
            didSet {
                btnBack.tintColor = NAVIGATION_BACK_COLOR
            }
        }
            
        @IBOutlet weak var lblNavigationTitle:UILabel! {
            didSet {
                lblNavigationTitle.text = REGISTER_PAGE_NAVIGATION_TITLE
                lblNavigationTitle.textColor = NAVIGATION_TITLE_COLOR
                lblNavigationTitle.backgroundColor = .clear
            }
        }
                    
    // ***************************************************************** // nav
    
    var whoIam:String!
    
    var myDeviceTokenIs:String!
    
    @IBOutlet weak var viewLoginBG:UIView! {
        didSet {
            viewLoginBG.layer.cornerRadius = 12
            viewLoginBG.clipsToBounds = true
            viewLoginBG.backgroundColor = .white //UIColor.init(red: 252.0/255.0, green: 252.0/255.0, blue: 252.0/255.0, alpha: 1)
            
            // border
            viewLoginBG.layer.borderColor = UIColor.lightGray.cgColor
            viewLoginBG.layer.borderWidth = 1.0

            // drop shadow
            viewLoginBG.layer.shadowColor = UIColor.black.cgColor
            viewLoginBG.layer.shadowOpacity = 0.8
            viewLoginBG.layer.shadowRadius = 3.0
            viewLoginBG.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
            
        }
    }
    
    @IBOutlet weak var btnDontHaveAnAccount:UIButton! {
        didSet {
            btnDontHaveAnAccount.setTitle("Already have an account ? - Sign In", for: .normal)
        }
    }
    
    @IBOutlet weak var btnForgotPassword:UIButton! {
        didSet {
            btnForgotPassword.setTitle("Forgot Password", for: .normal)
            btnForgotPassword.setTitleColor(BUTTON_BLUE_COLOR, for: .normal)
        }
    }
    
    @IBOutlet weak var btnSignIn:UIButton! {
        didSet {
            Dishu.buttonStyle(button: btnSignIn, bCornerRadius: 22, bBackgroundColor: BUTTON_BLUE_COLOR, bTitle: registrationPage_TEXT_DontHaveAnAccount, bTitleColor: .white)
        }
    }
    
    
    @IBOutlet weak var lblEmailTitle:UILabel! {
        didSet {
            lblEmailTitle.textColor = UIColor.init(red: 185.0/255.0, green: 185.0/255.0, blue: 185.0/255.0, alpha: 1)
        }
    }
    
    @IBOutlet weak var txtName:UITextField! {
        didSet {
            txtName.textColor = .black
            txtName.attributedPlaceholder = NSAttributedString(string: "name",
                                         attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
    }
    
    @IBOutlet weak var txtEmail:UITextField!{
        didSet {
            txtEmail.textColor = .black
            txtEmail.attributedPlaceholder = NSAttributedString(string: "email",
                                         attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
    }
    
    @IBOutlet weak var txtPassword:UITextField!{
        didSet {
            txtPassword.textColor = .black
            txtPassword.attributedPlaceholder = NSAttributedString(string: "password",
                                         attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
            txtPassword.isSecureTextEntry = true
        }
    }
    
    @IBOutlet weak var lblLoginMessage:UILabel! {
        didSet {
            lblLoginMessage.text = "Create an Account."
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.txtName.delegate = self
        self.txtEmail.delegate = self
        self.txtPassword.delegate = self
        
        self.view.backgroundColor = UIColor.init(red: 252.0/255.0, green: 252.0/255.0, blue: 252.0/255.0, alpha: 1)
        
        // MARK:- VIEW UP WHEN CLICK ON TEXT FIELD -
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        self.btnSignIn.addTarget(self, action: #selector(signInClickMethod), for: .touchUpInside)
        
        self.btnBack.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
        self.btnDontHaveAnAccount.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
        
        // self.txtName.text = "photographer 3"
        // self.txtEmail.text = "p3@gmail.com"
        // self.txtPassword.text = "123456Aa@"
    }

    @objc func backClickMethod() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    // MARK:- KEYBOARD WILL SHOW -
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }

    // MARK:- KEYBOARD WILL HIDE -
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    
    @objc func signInClickMethod() {
        
        
        
        if self.txtName.text == "" {
            
            let alert = UIAlertController(title: String("Alert"), message: String("Name should not be empty."), preferredStyle: UIAlertController.Style.alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
             }))
            self.present(alert, animated: true, completion: nil)
            
        } else if self.txtEmail.text == "" {
            
            let alert = UIAlertController(title: String("Alert"), message: String("Email should not be empty."), preferredStyle: UIAlertController.Style.alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
             }))
            self.present(alert, animated: true, completion: nil)
            
        } else if !isValidEmail(testStr: self.txtEmail.text!) {
            
            print("Validate EmailID")
            let alert = UIAlertController(title: "Alert!", message: "Please Enter a valid E-Mail Address.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
            self.present(alert, animated: true)
            
        } else if self.txtPassword.text == "" {
            
            let alert = UIAlertController(title: String("Alert"), message: String("Password should not be empty."), preferredStyle: UIAlertController.Style.alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
            }))
            self.present(alert, animated: true, completion: nil)
            
        } else if !validpassword(mypassword: self.txtPassword.text!) {
            
            print("hello")
            let alert = UIAlertController(title: "Password Alert", message: "Password setup rules: \n-one uppercase\n-one digit\n-one lowercase\n-one symbol\n-8 characters total", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
            self.present(alert, animated: true)
    
        } else {
            
            self.registrationWB()
            
        }
        
    }
    
    func isValidEmail(testStr:String) -> Bool {
        print("validate emilId: \(testStr)")
        let emailRegEx = "^(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?(?:(?:(?:[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+(?:\\.[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+)*)|(?:\"(?:(?:(?:(?: )*(?:(?:[!#-Z^-~]|\\[|\\])|(?:\\\\(?:\\t|[ -~]))))+(?: )*)|(?: )+)\"))(?:@)(?:(?:(?:[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)(?:\\.[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)*)|(?:\\[(?:(?:(?:(?:(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))\\.){3}(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))))|(?:(?:(?: )*[!-Z^-~])*(?: )*)|(?:[Vv][0-9A-Fa-f]+\\.[-A-Za-z0-9._~!$&'()*+,;=:]+))\\])))(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: testStr)
        return result
    }
    
    func validpassword(mypassword : String) -> Bool {
            // least one uppercase,
            // least one digit
            // least one lowercase
            // least one symbol
            //  min 8 characters total
            // let password = self.trimmingCharacters(in: CharacterSet.whitespaces)
            let passwordRegx = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&<>*~:`-]).{8,}$"
            let passwordCheck = NSPredicate(format: "SELF MATCHES %@",passwordRegx)
            return passwordCheck.evaluate(with: mypassword)

        
    }
    
    @objc func registrationWB() {
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
        
        
        // Create UserDefaults
        let defaults = UserDefaults.standard
        if let myString = defaults.string(forKey: "deviceFirebaseToken") {
            self.myDeviceTokenIs = myString

        }
        else {
            self.myDeviceTokenIs = "111111111111111111111"
        }
        
        self.view.endEditing(true)
        
        /*
         
         */
        
        let params = FullRegistration(action: "registration",
                                      fullName: String(self.txtName.text!),
                                      email: String(self.txtEmail.text!),
                                      password: String(txtPassword.text!),
                                      device: "iOS",
                                      deviceToken: String(myDeviceTokenIs),
                                      address: String(""),
                                      role : "Member")
        
        print(params as Any)
        
        AF.request(APPLICATION_BASE_URL,
                   method: .post,
                   parameters: params,
                   encoder: JSONParameterEncoder.default).responseJSON { response in
                    // debugPrint(response.result)
                    
                    switch response.result {
                    case let .success(value):
                        
                        let JSON = value as! NSDictionary
                        print(JSON as Any)
                        
                        var strSuccess : String!
                        strSuccess = JSON["status"]as Any as? String
                        
                        // var strSuccess2 : String!
                        // strSuccess2 = JSON["msg"]as Any as? String
                        
                        if strSuccess == String("success") {
                            print("yes")
                            ERProgressHud.sharedInstance.hide()
                           
                            var dict: Dictionary<AnyHashable, Any>
                            dict = JSON["data"] as! Dictionary<AnyHashable, Any>
                            
                             let defaults = UserDefaults.standard
                             defaults.setValue(dict, forKey: "keyLoginFullData")
                            
                            let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CompleteProfileUploadPictureId")
                            self.navigationController?.pushViewController(push, animated: true)
                            
                        } else {
                            print("no")
                            ERProgressHud.sharedInstance.hide()
                            
                            var strSuccess2 : String!
                            strSuccess2 = JSON["msg"]as Any as? String
                            
                            Utils.showAlert(alerttitle: String(strSuccess), alertmessage: String(strSuccess2), ButtonTitle: "Ok", viewController: self)
                            
                        }
                        
                    case let .failure(error):
                        print(error)
                        ERProgressHud.sharedInstance.hide()
                        
                        Utils.showAlert(alerttitle: SERVER_ISSUE_TITLE, alertmessage: SERVER_ISSUE_MESSAGE, ButtonTitle: "Ok", viewController: self)
                    }
        }
        
    }
}
